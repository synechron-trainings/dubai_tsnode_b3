import { AppDataSource } from "../config/data-source";
import { Employee } from "../models";
import { EmployeeEntity } from "../entities";

const employeeRepository = AppDataSource.getRepository(EmployeeEntity);

export const getEmployees = async (): Promise<Array<Employee>> => {
    const employeeEntities = await employeeRepository.find();
    return employeeEntities.map((employeeEntity) => new Employee(employeeEntity.employeeId, employeeEntity.name));
}

export const getEmployee = async (id: number): Promise<Employee> => {
    const employeeEntity = await employeeRepository.findOneBy({ employeeId: id });
    if (employeeEntity) {
        return new Employee(employeeEntity.employeeId, employeeEntity.name);
    }
    return null;
}

export const insertEmployee = async (employeeToInsert: Employee): Promise<Employee> => {
    const employeeEntity = employeeRepository.create({
        employeeId: employeeToInsert.id,
        name: employeeToInsert.name
    });

    await employeeRepository.save(employeeEntity);
    return new Employee(employeeEntity.employeeId, employeeEntity.name);
}

export const findAndUpdateEmployee = async (id: number, employeeToUpdate: Employee): Promise<Employee> => {
    let employeeEntity = await employeeRepository.findOneBy({ employeeId: id });
    if(employeeEntity) {
        // employeeEntity = employeeRepository.merge(employeeEntity, employeeToUpdate);
        employeeEntity.name = employeeToUpdate.name;
        await employeeRepository.save(employeeEntity);
        return new Employee(employeeEntity.employeeId, employeeEntity.name);
    }
    return null;
}

export const findAndDeleteEmployee = async (id: number): Promise<void> => {
    const employeeEntity = await employeeRepository.findOneBy({ employeeId: id });
    if(!employeeEntity) {
        throw new Error(`Employee with id ${id} not found`);
    }

    await employeeRepository.delete(employeeEntity);
}

export const employeDAO = {
    getEmployees,
    getEmployee,
    insertEmployee,
    findAndUpdateEmployee,
    findAndDeleteEmployee
}