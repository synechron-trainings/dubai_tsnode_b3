import { Request, Response } from 'express';
import { userDAO } from '../data-access';
import { userSchema } from '../validations';
import { User } from '../models';

export const getUsers = async (request: Request, response: Response) => {
    try {
        const page = request.query.page ? parseInt(request.query.page as string) : undefined;
        const limit = request.query.limit ? parseInt(request.query.limit as string) : undefined;

        const { users, total } = await userDAO.getUsers(page, limit);
        response.status(200).json({ users, totalPages: Math.ceil(total / (limit || 10)) });
    } catch (error) {
        response.status(500).json({ message: 'Failed to get users', error });
    }
}

export const getUserDetails = async (request: Request, response: Response) => {
    try {
        const id = parseInt(request.params.userid);
        const user = await userDAO.getUser(id);

        if (user) {
            response.status(200).json({ user });
        } else {
            response.status(404).json({ message: 'User not found' });
        }
    } catch (error) {
        response.status(500).json({ message: 'Failed to get user details', error });
    }
}

export const createUser = async (request: Request, response: Response) => {
    try {
        const value = await userSchema.validateAsync(request.body);
        const user = new User(value.userid, value.name, value.email);
        const insertedUser = await userDAO.insertUser(user);
        response.status(201).json({ message: 'User created successfully', user: insertedUser });
    } catch (error) {
        response.status(500).json({ message: 'Failed to create user', error });
    }
}

export const updateUser = async (request: Request, response: Response) => {
    try {
        const id = parseInt(request.params.userid);
        const value = await userSchema.validateAsync(request.body);
        const user = new User(value.userid, value.name, value.email);
        const updatedUser = await userDAO.findAndUpdateUser(id, user);

        if (updatedUser) {
            response.status(200).json({ message: 'User updated successfully', user: updatedUser });
        } else {
            response.status(404).json({ message: 'User not found' });
        }
    } catch (error) {
        response.status(500).json({ message: 'Failed to update user', error });
    }
}

export const deleteUser = async (request: Request, response: Response) => {
    try {
        const id = parseInt(request.params.userid);
        await userDAO.findAndDeleteUser(id);
        response.status(204).end();
    } catch (error) {
        response.status(500).json({ message: 'Failed to delete user', error });
    }
}