import { Column, Entity, ObjectId, ObjectIdColumn } from "typeorm";

@Entity({ name: 'Users' })
export class UserEntity {
    @ObjectIdColumn()
    _id: ObjectId;

    @Column({ unique: true })
    userId: number;

    @Column()
    username: string;

    @Column()
    email: string;
}