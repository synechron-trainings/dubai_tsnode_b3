import { Column, Entity, ObjectId, ObjectIdColumn } from "typeorm";

@Entity({ name: 'Employees' })
export class EmployeeEntity {
    @ObjectIdColumn()
    _id: ObjectId;

    @Column({ unique: true })
    employeeId: number;

    @Column()
    name: string;
}