{
    "openapi": "3.0.0",
    "info": {
        "title": "Synechron ENBD API",
        "description": "API documentation for Synechron ENBD",
        "version": "1.0.0",
        "contact": {
            "name": "Synechron ENBD",
            "email": "synechron@enbd.com"
        },
        "license": {
            "name": "MIT",
            "url": "https://opensource.org/licenses/MIT"
        }
    },
    "servers": [
        {
            "url": "http://localhost:3000",
            "description": "Local server"
        },
        {
            "url": "https://synechron-enbd.herokuapp.com",
            "description": "Production server"
        }
    ],
    "tags": [
        {
            "name": "User",
            "description": "User related endpoints"
        },
        {
            "name": "Employee",
            "description": "Employee related endpoints"
        }
    ],
    "paths": {
        "/employees": {
            "get": {
                "summary": "Retrieve a list of employees",
                "description": "Retrieve a list of employees from the database",
                "tags": [
                    "Employee"
                ],
                "responses": {
                    "200": {
                        "description": "List of employees",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/Employee"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "post": {
                "summary": "Add a new employee",
                "description": "Add a new employee to the database",
                "tags": [
                    "Employee"
                ],
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "eid": {
                                        "type": "integer",
                                        "description": "Employee Id",
                                        "example": "1"
                                    },
                                    "ename": {
                                        "type": "string",
                                        "description": "Employee Name",
                                        "example": "John Doe"
                                    }
                                },
                                "required": [
                                    "eid",
                                    "ename"
                                ]
                            }
                        }
                    }
                },
                "responses": {
                    "201": {
                        "description": "Employee added successfully",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/Employee"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/employees/{empid}": {
            "get": {
                "summary": "Retrieve an employee by ID",
                "description": "Retrieve an employee from the database by ID",
                "tags": [
                    "Employee"
                ],
                "parameters": [
                    {
                        "name": "empid",
                        "in": "path",
                        "description": "Employee ID",
                        "required": true,
                        "schema": {
                            "type": "integer",
                            "example": 1
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Returns a single employee",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/Employee"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Employee not found"
                    }
                }
            },
            "put": {
                "summary": "Update an employee by ID",
                "description": "Update an employee in the database by ID",
                "tags": [
                    "Employee"
                ],
                "parameters": [
                    {
                        "name": "empid",
                        "in": "path",
                        "description": "Employee ID",
                        "required": true,
                        "schema": {
                            "type": "integer",
                            "example": 1
                        }
                    }
                ],
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "eid": {
                                        "type": "integer",
                                        "description": "Employee Id",
                                        "example": "1"
                                    },
                                    "ename": {
                                        "type": "string",
                                        "description": "Employee Name",
                                        "example": "John Doe"
                                    }
                                },
                                "required": [
                                    "eid",
                                    "ename"
                                ]
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Employee updated successfully",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/Employee"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Employee not found"
                    }
                }
            },
            "delete": {
                "summary": "Delete an employee by ID",
                "description": "Delete an employee from the database by ID",
                "tags": [
                    "Employee"
                ],
                "parameters": [
                    {
                        "name": "empid",
                        "in": "path",
                        "description": "Employee ID",
                        "required": true,
                        "schema": {
                            "type": "integer",
                            "example": 1
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No Content"
                    },
                    "404": {
                        "description": "Employee not found"
                    }
                }
            }
        },
        "/users": {
            "get": {
                "summary": "Retrieve a list of users",
                "description": "Retrieve a list of users from the database",
                "tags": [
                    "User"
                ],
                "parameters": [
                    {
                        "in": "query",
                        "name": "page",
                        "required": false,
                        "description": "The page number for pagination",
                        "schema": {
                            "type": "integer",
                            "default": 1
                        }
                    },
                    {
                        "in": "query",
                        "name": "limit",
                        "required": false,
                        "description": "The number of items per page",
                        "schema": {
                            "type": "integer",
                            "default": 10
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "List of users",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/User"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "post": {
                "summary": "Create a new user",
                "description": "Create a new user to the database",
                "tags": [
                    "User"
                ],
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/User"
                            }
                        }
                    }
                },
                "responses": {
                    "201": {
                        "description": "User created successfully",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/User"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/users/{userid}": {
            "get": {
                "summary": "Retrieve a user by ID",
                "description": "Retrieve a user from the database by ID",
                "tags": [
                    "User"
                ],
                "parameters": [
                    {
                        "name": "userid",
                        "in": "path",
                        "description": "User ID",
                        "required": true,
                        "schema": {
                            "type": "integer",
                            "example": 1
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Returns a single user",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/User"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            },
            "put": {
                "summary": "Update a user by ID",
                "description": "Update a user in the database by ID",
                "tags": [
                    "User"
                ],
                "parameters": [
                    {
                        "name": "userid",
                        "in": "path",
                        "description": "User ID",
                        "required": true,
                        "schema": {
                            "type": "integer",
                            "example": 1
                        }
                    }
                ],
                "requestBody": {
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/User"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "User updated successfully",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/User"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            },
            "delete": {
                "summary": "Delete a user by ID",
                "description": "Delete a user from the database by ID",
                "tags": [
                    "User"
                ],
                "parameters": [
                    {
                        "name": "userid",
                        "in": "path",
                        "description": "User ID",
                        "required": true,
                        "schema": {
                            "type": "integer",
                            "example": 1
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "No Content"
                    },
                    "500": {
                        "description": "Failed to delete user"
                    }
                }
            }
        }
    },
    "components": {
        "schemas": {
            "Employee": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "number",
                        "description": "Employee ID",
                        "example": 1
                    },
                    "name": {
                        "type": "string",
                        "description": "Employee Name",
                        "example": "John Doe"
                    }
                }
            },
            "User": {
                "type": "object",
                "properties": {
                    "userid": {
                        "type": "integer",
                        "example": 1
                    },
                    "name": {
                        "type": "string",
                        "example": "John Doe"
                    },
                    "email": {
                        "type": "string",
                        "example": "john.doe@ti.com"
                    }
                },
                "required": [
                    "userid",
                    "name",
                    "email"
                ]
            }
        }
    }
}