import express from 'express';
import * as usersController from '../controllers/users-controller';

const router = express.Router();

router.get('/', usersController.getUsers);

router.get('/:userid', usersController.getUserDetails);

router.post('/', usersController.createUser);

router.put('/:userid', usersController.updateUser);

router.delete('/:userid', usersController.deleteUser);

export default router;