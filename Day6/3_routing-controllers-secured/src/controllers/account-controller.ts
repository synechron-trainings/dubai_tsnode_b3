import { Body, JsonController, Post } from "routing-controllers";
import * as jwt from 'jsonwebtoken';

@JsonController()
export class AccountController {
    @Post('/login')
    login(@Body() credentials: { username: string, password: string }) {
        if(credentials.username !== 'admin' && credentials.password !== 'admin') {
            throw new Error('Invalid credentials');
        } else {
            const token = jwt.sign({ username: credentials.username }, process.env.TOKEN_SECRET, { 
                expiresIn: '1h', 
                algorithm: 'HS256' 
            });
            return { token };
        }
    }
}