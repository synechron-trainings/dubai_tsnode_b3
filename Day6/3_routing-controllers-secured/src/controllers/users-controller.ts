import { userDAO } from '../data-access';
import { userSchema } from '../validations';
import { User } from '../models';
import { Body, Delete, Get, HttpCode, JsonController, NotFoundError, OnUndefined, Param, Post, Put, QueryParam, UseBefore } from 'routing-controllers';
import { authenticateJWT } from '../middlewares/jwt-auth-middleware';

@JsonController('/users')
@UseBefore(authenticateJWT)
export class UserController {
    @Get('/')
    async getUsers(@QueryParam("page") page: number, @QueryParam("limit") limit: number) {
        try {
            const { users, total } = await userDAO.getUsers(page, limit);
            return { users, totalPages: Math.ceil(total / (limit || 10)) };
        } catch (error) {
            throw new Error('Failed to get users');
        }
    }

    @Get('/:userid')
    async getUserDetails(@Param("userid") userid: number) {
        try {
            const user = await userDAO.getUser(userid);

            if (user) {
                return { user };
            } else {
                throw new NotFoundError('User not found');
            }
        } catch (error) {
            throw new Error('Failed to get user details');
        }

    }

    @Post('/')
    @HttpCode(201)
    async createUser(@Body() body: any) {
        try {
            const value = await userSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            const insertedUser = await userDAO.insertUser(user);
            return { message: 'User created successfully', user: insertedUser };
        } catch (error) {
            throw error;
        }
    }

    @Put('/:userid')
    async updateUser(@Param('userid') userid: number, @Body() body: any) {
        try {
            const value = await userSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            const updatedUser = await userDAO.findAndUpdateUser(userid, user);
            return { message: 'User updated successfully', user: updatedUser };
        } catch (error) {
            throw error;
        }
    }

    @Delete('/:userid')
    @OnUndefined(204)
    async deleteUser(@Param('userid') userid: number) {
        try {
            return await userDAO.findAndDeleteUser(userid);
        } catch (error) {
            throw new Error('Failed to delete user');
        }
    }
}