import express from 'express';
import favicon from 'serve-favicon';
import path from 'path';
import logger from 'morgan';
import 'dotenv/config';
import './config/data-source';
import cors from 'cors';

import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './config/swagger.json';

import 'reflect-metadata';
import { useExpressServer } from 'routing-controllers';

// Code for Creating Express Application Server using routing-controllers
// import { createExpressServer } from 'routing-controllers';
// const app = createExpressServer({
//     controllers: [`${__dirname}/controllers/**/*.ts`],
//     middlewares: [`${__dirname}/middlewares/**/*.ts`],
// });

const app = express();

app.set('view engine', 'pug');

const corsOptions = {
    origin: '*',                                            // Allow All Origins
    optionsSuccessStatus: 200,                              // some legacy browsers (IE11, various SmartTVs) choke on 204
    methods: 'GET, POST, PUT, DELETE, HEAD, PATCH',         // Allow only GET, POST, PUT, DELETE, HEAD, PATCH
    allowedHeaders: ['Content-Type', 'Authorization'],      // Allow only Content-Type and Authorization in Headers
    credentials: true                                       // This allows session cookies from the browser to pass through
};
app.use(cors(corsOptions));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

useExpressServer(app, {
    controllers: [`${__dirname}/controllers/**/*.ts`],
    middlewares: [`${__dirname}/middlewares/**/*.ts`],
});

export default app;