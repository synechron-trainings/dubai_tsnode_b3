import { Employee } from "./employee";
import { User } from "./user";

export { User, Employee };