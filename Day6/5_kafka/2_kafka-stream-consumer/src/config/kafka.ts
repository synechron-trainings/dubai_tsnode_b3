import { Consumer, EachMessagePayload, Kafka, logLevel } from "kafkajs";

export default class KafkaConsumer {
    private static instance: KafkaConsumer;
    private consumer: Consumer;

    private constructor() {
        this.consumer = this.createConsumer();
    }

    private createConsumer() {
        const kafka = new Kafka({
            clientId: 'my-api',
            brokers: ['localhost:9092'],
            logLevel: logLevel.ERROR
        });

        return kafka.consumer({ groupId: 'users-group' });
    }

    public static getInstance(): KafkaConsumer {
        if (!KafkaConsumer.instance) {
            KafkaConsumer.instance = new KafkaConsumer();
        }
        return KafkaConsumer.instance;
    }

    public async connect(): Promise<void> {
        try {
            await this.consumer.connect();
            console.log("Connected to Kafka Consumer");
            await this.consumer.subscribe({ topic: 'user-topic' });
            await this.consumer.run({
                eachMessage: this.processMessage.bind(this)
            });
        } catch (error) {
            console.error('Error Connecting to the Kafka Consumer', error);
        }
    }

    public async disconnect(): Promise<void> {
        try {
            await this.consumer.disconnect();
            console.log("Disconnected from Kafka Consumer");
        } catch (error) {
            console.error('Error Disconnecting from the Kafka Consumer', error);
        }
    }

    public async processMessage({ message }: EachMessagePayload): Promise<void> {
        const value = JSON.parse(message.value?.toString() || '');
        console.log('Received message:', value);
        // Process the received message as needed
    }
}