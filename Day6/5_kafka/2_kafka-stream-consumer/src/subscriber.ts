import KafkaConsumer from "./config/kafka";

export async function readMessages() {
    try {
        const consumer = KafkaConsumer.getInstance();
        await consumer.connect();
    } catch(error) {
        console.error('Error reading messages', error);
    }
}