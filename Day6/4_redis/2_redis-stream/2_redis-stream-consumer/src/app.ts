import express from 'express';
import logger from 'morgan';
import favicon from 'serve-favicon';
import path from 'path';
import 'dotenv/config';
import { subscribeToStream } from './subscriber';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

(async () => {
    try {
        await subscribeToStream();
    } catch (error) {
        console.error('Error subscribing to Redis stream:', error);
        process.exit(1);
    }
})();

export default app;