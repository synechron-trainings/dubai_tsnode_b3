import redisClient from "./config/redis";

export async function subscribeToStream(lastId = "$") {
    try {
        const results = await redisClient.xread("BLOCK", 0, "STREAMS", "user-stream", lastId);

        const [key, messages] = results[0];

        messages.forEach((message) => {
            console.log("Stream: %s --> Id: %s. Data: %O", key, message[0], message[1]);
            // Logic to process the message
        });

        await subscribeToStream(messages[messages.length - 1][0]);
    } catch (error) {
        throw new Error('Failed to subscribe to Redis stream');
    }
}