import { Server, createServer } from 'http';
import app from '../app';

const server: Server = createServer(app);

server.listen(process.env.PORT || 3000);

function onError(error: Error) {
    console.error('Error occurred while starting the server', error);
}

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Express Server started listening on port ${port}`);
}

server.on('error', onError);
server.on('listening', onListening);