import { userDAO } from '../data-access';
import { userSchema } from '../validations';
import { User } from '../models';
import { Body, Delete, Get, HttpCode, JsonController, NotFoundError, Param, Post, Put, QueryParam, UseBefore } from 'routing-controllers';
import { authenticateJWT } from '../middlewares/jwt-auth-middleware';
import redisClient from '../config/redis';
import { Redis } from 'ioredis';

@JsonController('/users')
@UseBefore(authenticateJWT)
export class UserController {
    private redisClient: Redis;

    constructor() {
        this.redisClient = redisClient;
    }

    @Get('/')
    async getUsers(@QueryParam("page") page: number, @QueryParam("limit") limit: number) {
        this.publishToUserStream({ action: 'getUsers', page, limit });

        try {
            let cacheKey = `users:${page}:${limit}`;

            if (!page || !limit) {
                cacheKey = 'users:all';
            }

            const cachedData = await this.redisClient.get(cacheKey);

            if (cachedData) {
                const cData = JSON.parse(cachedData);
                return { ...cData, message: 'Data from cache' };
            } else {
                const { users, total } = await userDAO.getUsers(page, limit);
                const data = { users, totalPages: Math.ceil(total / (limit || 10)), message: 'Data from DB' };
                this.redisClient.setex(cacheKey, 3600, JSON.stringify(data));     // Cache data for 1 hour
                return data;
            }
        } catch (error) {
            throw new Error('Failed to get users');
        }
    }

    @Get('/:userid')
    async getUserDetails(@Param("userid") userid: number) {
        this.publishToUserStream({ action: 'getUserDetails', userid });

        try {
            let cacheKey = `user:${userid}`;

            const cachedData = await this.redisClient.get(cacheKey);

            if (cachedData) {
                const cData = JSON.parse(cachedData);
                return { ...cData, message: 'Data from cache' };
            } else {
                const user = await userDAO.getUser(userid);

                if (user) {
                    const data = {
                        user,
                        message: 'Data from DB'
                    }

                    this.redisClient.setex(cacheKey, 3600, JSON.stringify(data));     // Cache data for 1 hour
                    return data;
                } else {
                    throw new NotFoundError('User not found');
                }
            }
        } catch (error) {
            throw new Error('Failed to get user details');
        }

    }

    @Post('/')
    @HttpCode(201)
    async createUser(@Body() body: any) {
        try {
            const value = await userSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            const insertedUser = await userDAO.insertUser(user);

            const keys = await this.redisClient.keys('users:*');
            await this.redisClient.del(...keys);

            this.publishToUserStream({ action: 'createUser', user: insertedUser });

            return { message: 'User created successfully', user: insertedUser };
        } catch (error) {
            throw error;
        }
    }

    @Put('/:userid')
    async updateUser(@Param('userid') userid: number, @Body() body: any) {
        try {
            const value = await userSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            const updatedUser = await userDAO.findAndUpdateUser(userid, user);

            let cacheKey = `user:${userid}`;
            await this.deleteIfExists(this.redisClient, cacheKey);

            const keys = await this.redisClient.keys('users:*');
            await this.redisClient.del(...keys);

            this.publishToUserStream({ action: 'updateUser', user: updatedUser });

            return { message: 'User updated successfully', user: updatedUser };
        } catch (error) {
            throw error;
        }
    }

    @Delete('/:userid')
    @HttpCode(204)
    async deleteUser(@Param('userid') userid: number) {
        try {
            await userDAO.findAndDeleteUser(userid);

            let cacheKey = `user:${userid}`;
            await this.deleteIfExists(this.redisClient, cacheKey);

            const keys = await this.redisClient.keys('users:*');
            await this.redisClient.del(...keys);

            this.publishToUserStream({ action: 'deleteUser', userid });

            return {};
        } catch (error) {
            throw new Error('Failed to delete user');
        }
    }

    private async deleteIfExists(redisClient, key) {
        const exists = await redisClient.exists(key);
        if (exists) {
            await redisClient.del(key);
            console.log(`Deleted key "${key}" from Redis`);
        } else {
            console.log(`Key "${key}" not found in Redis`);
        }
    }

    private async publishToUserStream(payload: any) {
        try {
            await this.redisClient.xadd('user-stream', '*', JSON.stringify(payload), `${payload.action} executed`);
        } catch (error) {
            throw new Error('Failed to publish to user stream');
        }
    }
}