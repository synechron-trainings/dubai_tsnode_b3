import { authenticateJWT } from '../middlewares/jwt-auth-middleware';
import { employeDAO } from '../data-access';
import { Employee } from '../models';
import { employeeSchema } from '../validations';
import { Body, Delete, Get, HttpCode, JsonController, NotFoundError, Param, Post, Put, UseBefore } from 'routing-controllers';

@JsonController('/employees')
@UseBefore(authenticateJWT)
export class EmployeeController {
    @Get('/')
    async getEmployees() {
        try {
            const employees = await employeDAO.getEmployees();
            return { employees };
        } catch (error) {
            throw new Error('Failed to get employees');
        }
    }

    @Get('/:empid')
    async getEmployeeDetails(@Param('empid') empid: number) {
        try {
            const employee = await employeDAO.getEmployee(empid);

            if (employee) {
                return { employee };
            } else {
                throw new NotFoundError('Employee not found');
            }
        } catch (error) {
            throw new Error('Failed to get employee details');
        }
    }

    @Post('/')
    @HttpCode(201)
    async createEmployee(@Body() body: any) {
        try {
            const { eid, ename } = await employeeSchema.validateAsync(body);
            const employee = new Employee(parseInt(eid), ename);
            const insertedEmployee = await employeDAO.insertEmployee(employee);
            return { message: 'Employee created successfully', employee: insertedEmployee };
        } catch (error) {
            throw new Error('Failed to create employee');
        }
    }

    @Put('/:empid')
    async updateEmployee(@Param('empid') empid: number, @Body() body: any) {
        try {
            const { eid, ename } = await employeeSchema.validateAsync(body);
            const employee = new Employee(parseInt(eid), ename);
            const updatedEmployee = await employeDAO.findAndUpdateEmployee(empid, employee);
            return { message: 'Employee updated successfully', employee: updatedEmployee };
        } catch (error) {
            throw new Error('Failed to update employee');
        }
    }

    @Delete('/:empid')
    @HttpCode(204) 
    async deleteEmployee(@Param('empid') empid: number) {
        try {
            await employeDAO.findAndDeleteEmployee(empid);
            return { };
        } catch (error) {
            throw new Error('Failed to delete employee');
        }
    }
}