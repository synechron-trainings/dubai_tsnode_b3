import { EmployeeEntity, UserEntity } from "../entities";
import { DataSource } from "typeorm";

export const AppDataSource: DataSource = new DataSource({
    type: "mongodb",
    url: process.env.MONGODB_URI,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    database: process.env.MONGODB_DB,
    entities: [EmployeeEntity, UserEntity],
    synchronize: true,
    logging: true
});

(function () {
    console.log("Connecting to the database, please wait...");

    AppDataSource.initialize().then(() => {
        console.log("Connected to the database successfully!");
    }).catch((error: Error) => {
        console.error("Failed to connect to the database!", error.message);
    });
})();