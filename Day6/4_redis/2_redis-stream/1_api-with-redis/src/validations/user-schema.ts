import Joi from 'joi';

export const userSchema = Joi.object({
    userid: Joi.number().required(),
    name: Joi.string().min(3).required(),
    email: Joi.string().email().required()
}).required();