import { Request, Response, NextFunction } from 'express';
import { UnauthorizedError } from 'express-jwt';
import { ExpressErrorMiddlewareInterface, HttpError, Middleware } from "routing-controllers";

@Middleware({ type: 'after' })
export class GlobalAppHttpErrorHandler implements ExpressErrorMiddlewareInterface {
    error(error: Error, request: Request, response: Response, next: NextFunction) {
        if(error instanceof UnauthorizedError) {
            response.status(401).json({ message: error.message });
        } else if(error instanceof HttpError) {
            response.status(error.httpCode).json({ message: error.message });
        } else {
            response.status(500).json({ message: error.message });
        }
    }
}