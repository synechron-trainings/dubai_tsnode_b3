import { expressjwt } from 'express-jwt';

export const authenticateJWT = expressjwt({ secret: process.env.TOKEN_SECRET, algorithms: ['HS256'] });

// import { Request, Response, NextFunction } from "express";
// import * as jwt from 'jsonwebtoken';

// export function authenticateJWT(req: Request, res: Response, next: NextFunction) {
//     const authHeader = req.headers.authorization;

//     if (authHeader) {
//         const token = authHeader.split(' ')[1];         // Bearer <token>

//         // Code to verify Token
//         jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
//             if (err) {
//                 res.sendStatus(403);              // Forbidden Access
//             }

//             // Attaching the user payload to the request, if needed
//             req.user = user;
//             next();
//         });
//     } else {
//         res.sendStatus(401);        // Unauthorized Access
//     }
// }
