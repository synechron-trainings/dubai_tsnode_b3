import { employeDAO } from "./employee-dao";
import { userDAO } from "./user-dao";

export { employeDAO, userDAO };