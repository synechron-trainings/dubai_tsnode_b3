import { employeeSchema } from "./employee-schema";
import { userSchema } from "./user-schema";

export { employeeSchema, userSchema };