import { Redis } from 'ioredis';

const MAX_RETRIES = process.env.REDIS_MAX_RETRIES ? parseInt(process.env.REDIS_MAX_RETRIES) : 3;
let retries = 0;

const redisClient = new Redis({
    host: process.env.REDIS_HOST || 'localhost',
    port: process.env.REDIS_PORT ? parseInt(process.env.REDIS_PORT) : 6379,
    password: process.env.REDIS_PASSWORD || undefined,
    retryStrategy: (times) => {
        if (times < MAX_RETRIES)
            return 5000;
        else
            return null;
    }
});

console.log('Connecting to Redis, please wait...');

redisClient.on('connect', () => {
    console.log('Connected to Redis successfully!');
});

redisClient.on('error', (error) => {
    retries++;
    if (retries < MAX_RETRIES) {
        console.error(`Error connecting to Redis server. Retry attempt ${retries}/${MAX_RETRIES}`);
    } else {
        console.error('Maximum retries exceeded. Exiting process.');
        process.exit(1);
    }
});

export default redisClient;