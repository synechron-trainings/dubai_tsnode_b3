import express from 'express';
import favicon from 'serve-favicon';
import path from 'path';
import logger from 'morgan';
import 'dotenv/config';
import './config/data-source';

import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './config/swagger.json';

import 'reflect-metadata';
import { useExpressServer } from 'routing-controllers';

// Code for Creating Express Application Server using routing-controllers
// import { createExpressServer } from 'routing-controllers';
// const app = createExpressServer({
//     controllers: [`${__dirname}/controllers/**/*.ts`],
//     middlewares: [`${__dirname}/middlewares/**/*.ts`],
// });

const app = express();

app.set('view engine', 'pug');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

useExpressServer(app, {
    controllers: [`${__dirname}/controllers/**/*.ts`],
    middlewares: [`${__dirname}/middlewares/**/*.ts`],
});

export default app;