import Joi from "joi";

export const employeeSchema = Joi.object({
    eid: Joi.number().required(),
    ename: Joi.string().min(2).required()
}).required();