import { Request, Response, NextFunction } from 'express';
import { ExpressErrorMiddlewareInterface, Middleware } from "routing-controllers";

@Middleware({ type: 'after' })
export class GlobalAppErrorHandler implements ExpressErrorMiddlewareInterface {
    error(error: Error, request: Request, response: Response, next: NextFunction) {
        response.locals.message = error.message;
        response.locals.error = request.app.get('env') === 'development' ? error : {};
        response.status(500);
        response.render('error', { pageTitle: 'Error Page' });
    }
}