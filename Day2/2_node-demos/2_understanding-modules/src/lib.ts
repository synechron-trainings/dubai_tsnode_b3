// console.log("This is the lib module");

export var fname = "Manish";
var lname = "Sharma";
console.log("Local:", lname);

export function check() {
    console.log("Check call from lib.ts file");
}

export class BankAccount {
    private static _bankName: string = "ICICI";

    constructor(private readonly _accNumber: number, private _accName: string) { }

    get BankName() {
        return BankAccount._bankName;
    }

    static set BankName(value: string) {
        BankAccount._bankName = value;
    }

    get AccountNumber(): number {
        return this._accNumber;
    }

    get AccountHolderName() {
        return this._accName;
    }
}