// console.log("Hello from Node");

// let i = 10;
// console.log(i);
// console.log(typeof i);

// console.log(process);
// console.log(process.cwd());
// console.log(process.env);

// console.log(__dirname);
// console.log(__filename);
// console.log(module);

// --------------------------
// require("./lib");
// import './lib';

// import * as lib from './lib';
// console.log(lib);

import { fname, check, BankAccount } from './lib';
console.log(fname);
check();

var a1 = new BankAccount(1, "Manish");
console.log(`Bank Name: ${a1.BankName}`);
console.log(`Account Number: ${a1.AccountNumber}`);
console.log(`Account Holder Name: ${a1.AccountHolderName}`);

var a2 = new BankAccount(2, "Abhijeet");
console.log(`\nBank Name: ${a2.BankName}`);
console.log(`Account Number: ${a2.AccountNumber}`);
console.log(`Account Holder Name: ${a2.AccountHolderName}`);