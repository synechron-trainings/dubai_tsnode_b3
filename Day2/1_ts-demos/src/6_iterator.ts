type Order = { id: number, name: string, qty: number };

// let ordersArray: Array<Order> = [
//     { id: 101, name: 'Mobile', qty: 1 },
//     { id: 102, name: 'Laptop', qty: 2 },
//     { id: 103, name: 'Tablet', qty: 3 }
// ];

// for (const item of ordersArray) {
//     console.log(item);
// }

// class Queue<T> {
//     private _data: T[] = [];

//     push(item: T) {
//         this._data.push(item);
//     }

//     pop(): T | undefined {
//         return this._data.shift();
//     }

//     [Symbol.iterator]() {
//         var i = 0, self = this;

//         return {
//             next: function () {
//                 var v, d = true;

//                 if (i < self._data.length) {
//                     v = self._data[i];
//                     i++;
//                     d = false;
//                 }

//                 return {
//                     done: d,
//                     value: v
//                 };
//             }
//         };
//     }
// }

class Queue<T> implements IterableIterator<T> {
    private _data: T[] = [];
    private _i: number = 0;

    push(item: T) {
        this._data.push(item);
    }

    pop(): T | undefined {
        return this._data.shift();
    }

    [Symbol.iterator](): IterableIterator<T> {
        return this;
    }

    next(...args: [] | [undefined]): IteratorResult<T, any> {
        if (this._i < this._data.length) {
            return {
                done: false,
                value: this._data[this._i++]
            };
        } else {
            return {
                done: true,
                value: undefined
            };
        }
    }
}

let ordersQ = new Queue<Order>();
ordersQ.push({ id: 101, name: 'Mobile', qty: 1 });
ordersQ.push({ id: 102, name: 'Laptop', qty: 2 });
ordersQ.push({ id: 103, name: 'Tablet', qty: 3 });

for (const item of ordersQ) {
    console.log(item);
}
