// const area = function (shape: { h: number, w?: number }) {
//     shape.w = shape.w || shape.h;
//     return shape.h * shape.w;
// }

// let s1 = { h: 20, w: 10 };
// console.log(area(s1));

// let s2 = { h: 20, w: 10, extra: 30 };
// console.log(area(s2));

// let s3 = { h: 20, extra: 30 };
// console.log(area(s3));

// // ---------------------------------- Type Alias
// type TShape = { h: number, w?: number };

// const area = function (shape: TShape) {
//     shape.w = shape.w || shape.h;
//     return shape.h * shape.w;
// }

// let s1: TShape = { h: 20, w: 10 };
// console.log(area(s1));

// // let s2: TShape = { h: 20, w: 10, extra: 30 };
// // console.log(area(s2));

// // let s3: TShape = { h: 20, extra: 30 };
// // console.log(area(s3));

// // ---------------------------------- Interface
// interface IShape {
//     h: number;
//     w?: number;
// }

// const area = function (shape: IShape) {
//     shape.w = shape.w || shape.h;
//     return shape.h * shape.w;
// }

// let s1: IShape = { h: 20, w: 10 };
// console.log(area(s1));

// // let s2: IShape = { h: 20, w: 10, extra: 30 };
// // console.log(area(s2));

// // let s3: IShape = { h: 20, extra: 30 };
// // console.log(area(s3));

// // ---------------------------------- With Functions
// interface IPerson {
//     name: string;
//     age: number;
//     greet: (msg: string) => string;
// }

// let p1: IPerson = {
//     name: 'Nag',
//     age: 35,
//     greet: function (msg: string) {
//         return 'Hello';
//     }
// };

// let p2: IPerson = {
//     name: 'Ria',
//     age: 3,
//     greet: function (msg: string) {
//         return 'Hola';
//     }
// };

// console.log(p1.greet('Good Morning'));
// console.log(p2.greet('Good Morning'));

// type TPerson = {
//     name: string;
//     age: number;
//     greet: (msg: string) => string;
// }

// let p3: TPerson = {
//     name: 'Nag',
//     age: 35,
//     greet: function (msg: string) {
//         return 'Hello';
//     }
// };

// let p4: TPerson = {
//     name: 'Ria',
//     age: 3,
//     greet: function (msg: string) {
//         return 'Hola';
//     }
// };

// console.log(p3.greet('Good Morning'));
// console.log(p4.greet('Good Morning'));

// // ------------------------------------ Difference between Type and Interface

// // type TShape = {
// //     height: number;
// // };

// // // Duplicate identifier 'TShape'.
// // type TShape = {
// //     width: number;
// // };

// // Interface Merging
// interface IShape {
//     height: number;
// }

// interface IShape {
//     width: number;
// }

// let s1: IShape = {
//     height: 10,
//     width: 20
// };

// // ----------------------------------------------------- Type Alias and Interface Used Together

// interface ICustomer {
//     doShopping(): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// type CustomerOrEmployee = ICustomer | IEmployee;        // Union Type

// var person1: CustomerOrEmployee = {
//     doShopping: function () {
//         return 'Shopping...';
//     }
// };

// type CustomerAndEmployee = ICustomer & IEmployee;        // Intersection Type

// var person2: CustomerAndEmployee = {
//     doShopping: function () {
//         return 'Shopping...';
//     },
//     doWork: function () {
//         return 'Working...';
//     }
// };