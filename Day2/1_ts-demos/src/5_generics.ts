// class Queue {
//     private _data: number[] = [];

//     push(item: number) {
//         this._data.push(item);
//     }

//     pop(): number | undefined {
//         return this._data.shift();
//     }
// }

// var numbersQ = new Queue();
// numbersQ.push(10);
// numbersQ.push(20);
// numbersQ.push(30);

// console.log(numbersQ.pop());
// console.log(numbersQ.pop());
// console.log(numbersQ.pop());

// // ---------------------------------------

// class Queue {
//     private _data: any[] = [];

//     push(item: any) {
//         this._data.push(item);
//     }

//     pop(): any | undefined {
//         return this._data.shift();
//     }
// }

// var numbersQ = new Queue();
// numbersQ.push(10);
// numbersQ.push(20);
// numbersQ.push(30);

// console.log(numbersQ.pop());
// console.log(numbersQ.pop());
// console.log(numbersQ.pop());

// type Order = { id: number, name: string, qty: number };

// var ordersQ = new Queue();
// ordersQ.push({ id: 101, name: 'Mobile', qty: 1 });
// ordersQ.push({ id: 102, name: 'Laptop', qty: 2 });
// ordersQ.push({ id: 103, name: 'Tablet', qty: 3 });
// ordersQ.push(10);

// console.log(ordersQ.pop().name.toUpperCase());
// console.log(ordersQ.pop());
// console.log(ordersQ.pop());
// console.log(ordersQ.pop().name.toUpperCase());

// // ---------------------------------------------- Generics
// // Generics allows you to create a component that can work over a variety of types
// // rather than a single one, without loosing type safety and intellisense

// class Queue<T> {
//     private _data: T[] = [];

//     push(item: T) {
//         this._data.push(item);
//     }

//     pop(): T | undefined {
//         return this._data.shift();
//     }
// }

// var numbersQ = new Queue<number>();
// numbersQ.push(10);
// numbersQ.push(20);
// numbersQ.push(30);

// console.log(numbersQ.pop());
// console.log(numbersQ.pop());
// console.log(numbersQ.pop());

// type Order = { id: number, name: string, qty: number };

// var ordersQ = new Queue<Order>();
// ordersQ.push({ id: 101, name: 'Mobile', qty: 1 });
// ordersQ.push({ id: 102, name: 'Laptop', qty: 2 });
// ordersQ.push({ id: 103, name: 'Tablet', qty: 3 });

// console.log(ordersQ.pop()?.name.toUpperCase());
// console.log(ordersQ.pop());
// console.log(ordersQ.pop());
// console.log(ordersQ.pop()?.name.toUpperCase());


// --------------------------------------------------- Generics with constraints
// Create a Generic Method to merge 2 objects

// // function merge<T1, T2>(obj1: T1, obj2: T2): T1 & T2 {
// //     return { ...obj1, ...obj2 };
// // }

// function merge<T1 extends object, T2 extends object>(obj1: T1, obj2: T2): T1 & T2 {
//     return { ...obj1, ...obj2 };
// }

// let rObj1 = merge<{id: number, name: string}, {price: number}>({ id: 1, name: 'Product 1' }, { price: 100 });
// console.log(rObj1);

// // let rObj2 = merge<number, boolean>(10, true);           // Error - Type 'number' does not satisfy the constraint 'object'.
// // console.log(rObj2);

// ------------------------------------

// interface IShape {
//     draw(): void;
// }

// class Circle implements IShape {
//     draw(): void {
//         console.log("Circle Drawn");
//     }
// }

// class Square implements IShape {
//     draw(): void {
//         console.log("Square Drawn");
//     }
// }

// function drawShapes<T extends IShape>(shapes: T[]) {
//     shapes.forEach(shape => shape.draw());
// }

// drawShapes([new Circle(), new Square()]);

// ------------------------------------

// var person = { id: 1, name: "Person 1", age: 30 };

// // function getPropertyValue<T, K>(obj: T, key: K): T[K] {
// //     return obj[key];                // Error - Type 'K' cannot be used to index type 'T'
// // }

// function getPropertyValue<T, K extends keyof T>(obj: T, key: K): T[K] {
//     return obj[key];                // Error - Type 'K' cannot be used to index type 'T'
// }

// console.log(getPropertyValue(person, "id"));
// console.log(getPropertyValue(person, "name"));
// console.log(getPropertyValue(person, "age"));

// ------------------------------------

class Car {
    constructor(public name: string) { }

    drive() {
        console.log(`${this.name} is driving`);
    }
}

class Customer {
    constructor(public name: string) { }

    buy() {
        console.log(`${this.name} is buying`);
    }
}

// let c = new Car("Audi");
// c.drive();

function createInstance<T>(cls: { new(...args: any[]): T }) {
    return new cls("BMW");
}

// let c = createInstance(Car);
// c.drive();

// let cust = createInstance(Customer);
// cust.buy();

let c = createInstance<Car>(Car);
c.drive();

let cust = createInstance<Customer>(Customer);
cust.buy();