// interface IPerson {
//     name: string;
//     age: number;
//     greet(msg: string): string;
// }

// class Person implements IPerson {
//     constructor(public name: string, public age: number) { }

//     greet(msg: string): string {
//         return `Hello ${this.name}, ${msg}`;
//     }
// }

// let p1: IPerson = new Person("Manish", 35);
// console.log(p1.greet('Good Morning'));

// let p2: Person = new Person("Abhijeet", 40);
// console.log(p2.greet('Good Morning'));

// // ----------------------------------- Multiple Interface Implementation

// interface IPerson {
//     name: string;
//     age: number;
//     greet(msg: string): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// class Person implements IPerson, IEmployee {
//     constructor(public name: string, public age: number) { }

//     greet(msg: string): string {
//         return `Hello ${this.name}, ${msg}`;
//     }

//     doWork(): string {
//         return 'Learning TypeScript';
//     }
// }


// let p1: Person = new Person("Manish", 35);
// console.log(p1.greet('Good Morning'));
// console.log(p1.doWork());

// // ----------------------------------- Interface Extraction

// interface IPerson {
//     name: string;
//     age: number;
//     greet(msg: string): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// interface ICustomer {
//     doShopping(): string;
// }

// class Person implements IPerson, IEmployee, ICustomer {
//     constructor(public name: string, public age: number) { }

//     greet(msg: string): string {
//         return `Hello ${this.name}, ${msg}`;
//     }

//     doWork(): string {
//         return 'Learning TypeScript';
//     }

//     doShopping(): string {
//         return `Shopping in progress`;
//     }
// }

// let p1: Person = new Person("Manish", 35);
// console.log(p1.greet('Good Morning'));
// console.log(p1.doWork());
// console.log(p1.doShopping());

// // Interface Extraction
// let p2: IPerson = new Person("Abhijeet", 40);
// console.log(p2.greet('Good Morning'));

// let e1: IEmployee = new Person("Abhijeet", 40);
// console.log(e1.doWork());

// let c1: ICustomer = new Person("Abhijeet", 40);
// console.log(c1.doShopping());

// ----------------------------------- Interface can extend another Interface(s)

interface IPerson {
    name: string;
    age: number;
    greet(msg: string): string;
}

interface IEmployee extends IPerson {
    doWork(): string;
}

interface ICustomer extends IPerson {
    doShopping(): string;
}

class Person implements IPerson, IEmployee, ICustomer {
    constructor(public name: string, public age: number) { }

    greet(msg: string): string {
        return `Hello ${this.name}, ${msg}`;
    }

    doWork(): string {
        return 'Learning TypeScript';
    }

    doShopping(): string {
        return `Shopping in progress`;
    }
}

let e1: IEmployee = new Person("Abhijeet", 40);
console.log(e1.greet('Good Morning'));
console.log(e1.doWork());

let c1: ICustomer = new Person("Abhijeet", 40);
console.log(c1.greet('Good Morning'));
console.log(c1.doShopping());