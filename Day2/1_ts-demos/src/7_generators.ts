// // Eager Execution
// function idNormal() {
//     console.log("Id is created via normal function");
// }

// // Lazy Execution
// function* idGenerator() {
//     console.log("Id is created via generator function");
// }

// // idNormal();
// // idGenerator();

// var genObj = idGenerator();
// // console.log(genObj);
// console.log(genObj.next());

// -----------------------------------------

// function* idGenerator() {
//     console.log("Generator Function execution satrted");
//     return 100;
// }

// function* idGenerator() {
//     console.log("Generator Function execution satrted");
//     yield 100;
// }

// function* idGenerator() {
//     console.log("Generator Function execution started");
//     yield 1;
//     yield 2;
//     yield 3;
//     console.log("Generator Function execution completed");
// }

// var genObj = idGenerator();
// console.log(genObj.next());
// console.log(genObj.next());
// console.log(genObj.next());
// console.log(genObj.next());


// function* idGenerator() {
//     console.log("Generator Function execution started");
//     yield 1;
//     console.log("Second value is yielded");
//     yield 2;
//     console.log("Third value is yielded");
//     yield 3;
//     console.log("Generator Function execution completed");
// }

// var genObj = idGenerator();
// console.log(genObj.next());
// console.log(genObj.next());
// console.log(genObj.next());
// console.log(genObj.next());

// --------------------------------------

class GQueue<T> {
    private _data: T[] = [];
    private _i: number = 0;

    push(item: T) {
        this._data.push(item);
    }

    pop(): T | undefined {
        return this._data.shift();
    }

    // *[Symbol.iterator]() {
    //     for (let i = 0; i < this._data.length; i++) {
    //         yield this._data[i];
    //     }
    // }

    *[Symbol.iterator]() {
        yield* this._data;
    }
}

let ordersGQ = new GQueue<Order>();
ordersGQ.push({ id: 101, name: 'Mobile', qty: 1 });
ordersGQ.push({ id: 102, name: 'Laptop', qty: 2 });
ordersGQ.push({ id: 103, name: 'Tablet', qty: 3 });

for (const item of ordersGQ) {
    console.log(item);
}