// // Synchronous code
// // console.log("Hello One");
// // console.log("Hello Two");
// // console.log("Hello Three");

// // Asynchronous code
// // console.log("Hello One");
// // setTimeout(() => {
// //     console.log("Hello Two");
// // }, 0);
// // console.log("Hello Three");

// // ---------------------------------------- Readline Module
// // The readline module provides an interface for reading data from a Readable stream (such as process.stdin) one line at a time.
// // The readline module is useful for creating command-line interfaces.

// import * as readline from 'readline';
// // console.log(readline);

// const rl = readline.createInterface({
//     input: process.stdin,       // readable stream
//     output: process.stdout      // writable stream
// });

// // rl.question('What is your name? ', (name) => {
// //     console.log(`Hello ${name}!`);
// //     rl.close();
// // });

// // console.log("I am after readline.question code...");

// // rl.question('Enter the first number: ', (input1) => {
// //     rl.question('Enter the second number: ', (input2) => {
// //         let sum = parseInt(input1) + parseInt(input2);
// //         console.log(`The sum of ${input1} and ${input2} is ${sum}`);
// //         rl.close();
// //     });
// // });

// // -------------------------------------------
// // Convert the above code to use promise

// function enterNumberOne(): Promise<string> {
//     return new Promise((resolve, reject) => {
//         rl.question('Enter the first number: ', (input) => {
//             resolve(input);
//         });
//     });
// }

// function enterNumberTwo(): Promise<string> {
//     return new Promise((resolve, reject) => {
//         rl.question('Enter the second number: ', (input) => {
//             resolve(input);
//         });
//     });
// }

// function add([n1, n2]: [string, string]) {
//     var sum = parseInt(n1) + parseInt(n2);
//     console.log(`The sum of ${n1} and ${n2} is ${sum}`);
//     rl.close();
// }

// enterNumberOne().then((n1) => {
//     enterNumberTwo().then((n2) => {
//         add([n1, n2]);
//     });
// });

// Synchronous code
// console.log("Hello One");
// console.log("Hello Two");
// console.log("Hello Three");

// Asynchronous code
// console.log("Hello One");
// setTimeout(() => {
//     console.log("Hello Two");
// }, 0);
// console.log("Hello Three");

// ---------------------------------------- Readline Module
// The readline module provides an interface for reading data from a Readable stream (such as process.stdin) one line at a time.
// The readline module is useful for creating command-line interfaces.

import * as readline from 'readline';
// console.log(readline);

const rl = readline.createInterface({
    input: process.stdin,       // readable stream
    output: process.stdout      // writable stream
});

// rl.question('What is your name? ', (name) => {
//     console.log(`Hello ${name}!`);
//     rl.close();
// });

// console.log("I am after readline.question code...");

// rl.question('Enter the first number: ', (input1) => {
//     rl.question('Enter the second number: ', (input2) => {
//         let sum = parseInt(input1) + parseInt(input2);
//         console.log(`The sum of ${input1} and ${input2} is ${sum}`);
//         rl.close();
//     });
// });

// // ------------------------------------------- Promise Chaining
// function enterNumberOne(): Promise<string> {
//     return new Promise((resolve, reject) => {
//         rl.question('Enter the first number: ', (input) => {
//             resolve(input);
//         });
//     });
// }

// function enterNumberTwo(n1: string): Promise<[string, string]> {
//     return new Promise((resolve, reject) => {
//         rl.question('Enter the second number: ', (input) => {
//             resolve([n1, input]);
//         });
//     });
// }

// function add([n1, n2]: [string, string]) {
//     var sum = parseInt(n1) + parseInt(n2);
//     console.log(`The sum of ${n1} and ${n2} is ${sum}`);
//     rl.close();
// }

// enterNumberOne().then(enterNumberTwo).then(add);

// --------------------------------------------- Single Input Method
function enterNumber(message: string): Promise<number> {
    return new Promise<number>((resolve, reject) => {
        rl.question(message, (input) => {
            resolve(parseInt(input));
        });
    });
}

// enterNumber('Enter the first number: ').then((n1) => {
//     enterNumber('Enter the second number: ').then((n2) => {
//         let sum = n1 + n2;
//         console.log(`The sum of ${n1} and ${n2} is ${sum}`);
//         rl.close();
//     });
// });

// ------------------------ Async/Await

(async function () {
    let n1 = await enterNumber('Enter the first number: ');
    let n2 = await enterNumber('Enter the second number: ');
    let sum = n1 + n2;
    console.log(`The sum of ${n1} and ${n2} is ${sum}`);
    rl.close();
})();