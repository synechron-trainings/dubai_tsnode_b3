You are tasked with designing a BankAccount class that adheres to the SOLID principles. 
The class should facilitate depositing and withdrawing funds from an account while maintaining 
the integrity of the account balance. 

Additionally, the class should integrate with external utilities to send SMS and email notifications after each transaction.

Requirements:
1. Implement a BankAccount class with methods for depositing and withdrawing funds.
2. Ensure that withdrawing funds from the account displays an error message if the withdrawal amount exceeds the available balance.
3. Display a message after each deposit and withdrawal, showing the current balance of the account.
4. Integrate with an external SMS utility to send a notification after each deposit or withdrawal.
5. Integrate with an external Email utility to send a notification after each deposit or withdrawal.

Your task is to design and implement the BankAccount class, adhering to the SOLID principles and fulfilling 
the requirements outlined above.