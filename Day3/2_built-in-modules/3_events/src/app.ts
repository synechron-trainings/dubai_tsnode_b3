import { StringEmitter } from "./string-emitter";

const stringEmitter = StringEmitter.getInstance();

// const str = stringEmitter.getString();
// console.log(str);

// setInterval(() => {
//     const str = stringEmitter.getString();
//     console.log(str);
// }, 2000);

// Call 1 - 1000
// Call 2 - 3000
// Call 3 - 1000

// --------------------------------------------
// stringEmitter.pushString((s: string) => {
//     console.log(s);
// });

// stringEmitter.pushString((s: string) => {
//     console.log(`S1: ${s}`);
// });

// stringEmitter.pushString((s: string) => {
//     console.log(`S2: ${s}`);
// });

// --------------------------------------------

// stringEmitter.on('stringEmitted', (s: string) => {
//     console.log(s);
// });

stringEmitter.on('stringEmitted', (s: string) => {
    console.log(`S1: ${s}`);
});

// stringEmitter.on('stringEmitted', (s: string) => {
//     console.log(`S2: ${s}`);
// });

let count = 0;

function printString(s: string) {
    console.log(`S2: ${s}`);
    count++;
    if(count === 3) {
        stringEmitter.off('stringEmitted', printString);
    }
}

stringEmitter.on('stringEmitted', printString);