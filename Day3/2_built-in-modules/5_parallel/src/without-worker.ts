import * as fs from 'fs';
import * as path from 'path';
import * as crypto from 'crypto';

const filePath = path.join(process.cwd(), 'files', 'large_file.txt');
const numIterations = 4;            // To simulate processing the equivalent of 4 workers' workload

console.time('Processing the file without worker');

// Function to simulate CPU-intensive processing (mock encryption)
function mockEncryption(data: string): string {
    let encrypted = data;

    for (let i = 0; i < 1000000; i++) {
        encrypted = crypto.createHash('sha256').update(encrypted).digest('hex');
    }

    return encrypted;
}

function readAndProcessFile(iteration) {
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            const encryptedData = mockEncryption(data);
            console.log(`Processing complete for iteration ${iteration}.`);
            if (iteration < numIterations) {
                readAndProcessFile(iteration + 1);
            } else {
                console.log(encryptedData.length);
                console.timeEnd('Processing the file without worker');
            }
        }
    });
}

readAndProcessFile(1);