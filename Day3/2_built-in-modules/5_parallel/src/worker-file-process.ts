import * as fs from 'fs';
import * as crypto from 'crypto';
import * as Worker from 'worker_threads';

const filePath = Worker.workerData.path;

function mockEncryption(data: string): string {
    let encrypted = data;

    for (let i = 0; i < 1000000; i++) {
        encrypted = crypto.createHash('sha256').update(encrypted).digest('hex');
    }

    return encrypted;
}

fs.readFile(filePath, 'utf-8', (err, data) => {
    if (err) {
        console.log(err);
    } else {
        const encryptedData = mockEncryption(data);
        Worker.parentPort.postMessage(encryptedData);
    }
});