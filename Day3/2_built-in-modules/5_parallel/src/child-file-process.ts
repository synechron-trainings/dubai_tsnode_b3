import * as fs from 'fs';
import * as crypto from 'crypto';

interface ProcessMessage {
    path: string;
}

const args = JSON.parse(process.argv[2]) as ProcessMessage;
const filePath = args.path;

function mockEncryption(data: Buffer): Buffer {
    let encrypted = data;

    for (let i = 0; i < 1000000; i++) {
        encrypted = crypto.createHash('sha256').update(encrypted).digest();
    }

    return encrypted;
}

fs.readFile(filePath, (err, data) => {
    if (err) {
        if (process.send) {
            process.send({ error: err.message });
        }
        return;
    } else {
        const encryptedData = mockEncryption(data);
        if (process.send) {
            process.send({ processed: 'done', data: encryptedData.toString('hex') });
        }
    }
});