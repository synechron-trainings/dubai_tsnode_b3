import * as fs from 'fs';
import * as path from 'path';

// const filePath = path.join(process.cwd(), 'files', 'file1.txt');

// Read file Synchrnously
// try {
//     const data = fs.readFileSync(filePath, 'utf-8');
//     console.log(data);
// } catch (err) {
//     console.log(err);
// }

// Read file Asynchrnously
// fs.readFile(filePath, 'utf-8', (err, data) => { 
//     if(err) {
//         console.log(err);
//     } else {
//         console.log(data);
//     }
// });

// // Write file Asynchrnously
// const filePath = path.join(process.cwd(), 'files', 'file2.txt');
// let message = "Hello from Node Application\n";

// fs.writeFile(filePath, message, 'utf-8', (err) => { 
//     if(err) {
//         console.log(err);
//     } else {
//         console.log('File written successfully');
//     }
// });

// Append file Asynchrnously
const filePath = path.join(process.cwd(), 'files', 'file3.txt');
let message = "Hello from Node Application\n";

fs.appendFile(filePath, message, 'utf-8', (err) => { 
    if(err) {
        console.log(err);
    } else {
        console.log('File appended successfully');
    }
});

console.log("Completed and Waiting....");