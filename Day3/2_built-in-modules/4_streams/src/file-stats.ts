import { stat } from 'fs';

function getFileSize(filePath: string): Promise<number> {
    return new Promise((resolve, reject) => {
        stat(filePath, (err, stats) => {
            if (err) {
                reject(err);
                return;
            } else {
                resolve(stats.size);
            }
        });
    });
}

export function compareFileSizes(originalFilePath: string, compressedFilePath: string): void {
    Promise.all([
        getFileSize(originalFilePath),
        getFileSize(compressedFilePath)
    ]).then(([originalSize, compressedSize])=>{
        console.log(`Original File Size: ${originalSize} bytes`);
        console.log(`Compressed File Size: ${compressedSize} bytes`);
        console.log(`Compression Saved: ${originalSize - compressedSize} bytes`);
        console.log(`Compression Ratio: ${(compressedSize/originalSize).toFixed(2)}`);
    }).catch(err => console.log(err));
}