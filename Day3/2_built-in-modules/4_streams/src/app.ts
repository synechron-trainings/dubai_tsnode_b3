// import * as fs from 'fs';
// import * as path from 'path';

// const filePath = path.join(process.cwd(), 'files', 'file1.txt');

// // // Read the entire file into memory and then callback will be executed
// // fs.readFile(filePath, 'utf-8', (err, data) => { 
// //     if(err) {
// //         console.log(err);
// //     } else {
// //         console.log(data);
// //     }
// // });

// // const readStream = fs.createReadStream(filePath, { 
// //     encoding: 'utf-8',
// //     highWaterMark: 1024
// // });

// const readStream = fs.createReadStream(filePath, { encoding: 'utf-8' });

// readStream.on('open', () => {
//     console.log('Stream is open');
// });

// readStream.on('error', (err: NodeJS.ErrnoException) => {
//     console.log(err.message);
// });

// readStream.on('data', (chunk: string) => {
//     console.log(chunk);
// });

// readStream.on('end', () => {
//     console.log('Stream is ended');
// });

// // ---------------------------------------------- File Copy

// import * as fs from 'fs';
// import * as path from 'path';

// const readFilePath = path.join(process.cwd(), 'files', 'file1.txt');
// const writeFilePath = path.join(process.cwd(), 'files', 'file2.txt');

// const readStream = fs.createReadStream(readFilePath, { encoding: 'utf-8' });
// const writeStream = fs.createWriteStream(writeFilePath, { encoding: 'utf-8' });

// readStream.on('error', (err: NodeJS.ErrnoException) => {
//     console.error('Error reading file:', err.message);
// });

// writeStream.on('error', (err: NodeJS.ErrnoException) => {
//     console.error('Error writing file:', err.message);
// });

// readStream.on('data', (chunk: string) => {
//     writeStream.write(chunk);
// });

// readStream.on('end', () => {
//     console.log('File Copied...');
// });

// // ---------------------------------------------- File Copy using Pipe

// import * as fs from 'fs';
// import * as path from 'path';

// const readFilePath = path.join(process.cwd(), 'files', 'file1.txt');
// const writeFilePath = path.join(process.cwd(), 'files', 'file3.txt');

// const readStream = fs.createReadStream(readFilePath, { encoding: 'utf-8' });
// const writeStream = fs.createWriteStream(writeFilePath, { encoding: 'utf-8' });

// readStream.pipe(writeStream).on('finish', () => {
//     console.log('File Copied...');
// });

// ---------------------------------------------- File Compression using Pipe

import * as fs from 'fs';
import * as path from 'path';
import * as zlib from 'zlib';
import { compareFileSizes } from './file-stats';

const readFilePath = path.join(process.cwd(), 'files', 'file1.txt');
const writeFilePath = path.join(process.cwd(), 'files', 'file1.txt.gz');

const readStream = fs.createReadStream(readFilePath, { encoding: 'utf-8' });
const writeStream = fs.createWriteStream(writeFilePath, { encoding: 'utf-8' });

readStream
    .pipe(zlib.createGzip())
    .pipe(writeStream)
    .on('finish', () => {
        console.log('File Compressed...');
        compareFileSizes(readFilePath, writeFilePath);
    });