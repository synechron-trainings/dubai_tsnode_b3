// Import module for side effect (run the global code in the module), without importing any of its exports
// import './logger';

// Import Module for its exports
// import * as logger1 from './logger';
// import * as logger2 from './logger';
// console.log(logger1 === logger2);

// ----------------------------------
// const logger1 = require('./logger1');
// import * as logger from './logger';
// console.log(logger);

// ---------------------------------- Singleton
// import { loggerService } from './logger-singleton';
// loggerService.log("Hello from App");

// ---------------------------------- Factory

import { DBLoggerFactory, LoggerFactory, FileLoggerFactory, MDBLoggerFactory } from "./logger-factory"; 

function clientCode(loggerFactory: typeof LoggerFactory) {
    const logger = loggerFactory.createLogger();
    logger.log("Hello from App");
}

clientCode(DBLoggerFactory);
clientCode(FileLoggerFactory);
clientCode(MDBLoggerFactory);