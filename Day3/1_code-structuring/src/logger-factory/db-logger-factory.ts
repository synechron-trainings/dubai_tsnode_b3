import { DBLogger } from "./db-logger";
import { ILogger } from "./i-logger";
import { LoggerFactory } from "./logger-factory";

export class DBLoggerFactory extends LoggerFactory {
    static createLogger(): ILogger {
        return new DBLogger();
    }
}