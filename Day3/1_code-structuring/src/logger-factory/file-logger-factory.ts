import { FileLogger } from "./file-logger";
import { ILogger } from "./i-logger";
import { LoggerFactory } from "./logger-factory";

export class FileLoggerFactory extends LoggerFactory {
    static createLogger(): ILogger {
        return new FileLogger();
    }
}