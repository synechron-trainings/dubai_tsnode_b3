import { ILogger } from "./i-logger";
import { LoggerFactory } from "./logger-factory";
import { MDBLogger } from "./mdb-logger";

export class MDBLoggerFactory extends LoggerFactory {
    static createLogger(): ILogger {
        return new MDBLogger();
    }
}