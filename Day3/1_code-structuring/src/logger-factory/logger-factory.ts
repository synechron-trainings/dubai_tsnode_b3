import { ILogger } from "./i-logger";

export abstract class LoggerFactory {
    static createLogger(): ILogger {
        throw new Error("Method not implemented.");
    }
}