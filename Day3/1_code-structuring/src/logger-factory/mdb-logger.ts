import { ILogger } from "./i-logger";

export class MDBLogger implements ILogger {
    log(message: string): void {
        console.log(`${message}, logged in mongodb database`);
    }
}