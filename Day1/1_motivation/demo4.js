console.log("Hello from Demo Four");
var CPerson = /** @class */ (function () {
    function CPerson(name) {
        this._name = name;
    }
    CPerson.prototype.getName = function () {
        return this._name;
    };
    CPerson.prototype.setName = function (name) {
        this._name = name;
    };
    return CPerson;
}());
var person1 = new CPerson("Manish");
console.log(person1.getName());
person1.setName("Abhijeet");
console.log(person1.getName());
var person2 = new CPerson("Subodh");
console.log(person2.getName());
person2.setName("Ramakant");
console.log(person2.getName());
console.log(person1);
console.log(person2);
// 136 bytes (68 bytes each)
