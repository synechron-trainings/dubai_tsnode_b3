console.log("Hello from Demo Two");

var Person = (function () {
    function Person(name) {
        this._name = name;
    }

    Person.prototype.getName = function () {
        return this._name;
    }

    Person.prototype.setName = function (name) {
        this._name = name;
    }

    return Person;
})();

var person1 = new Person("Manish");
console.log(person1.getName());
person1.setName("Abhijeet");
console.log(person1.getName());

var person2 = new Person("Subodh");
console.log(person2.getName());
person2.setName("Ramakant");
console.log(person2.getName());

console.log(person1);
console.log(person2);

// console.log(Person.prototype);
// console.log(p1.__proto__);           // Dunder Proto
// console.log(p2.__proto__);           // Dunder Proto

console.log(Person.prototype === person1.__proto__);
console.log(Person.prototype === person2.__proto__);
console.log(person1.__proto__ === person2.__proto__);

// 136 bytes (68 bytes / instance)