console.log("Hello from Demo Four");

class CPerson {
    private _name: string;

    constructor(name: string) {
        this._name = name;
    }

    getName(): string {
        return this._name;
    }

    setName(name: string): void {
        this._name = name;
    }
}

var person1 = new CPerson("Manish");
console.log(person1.getName());
person1.setName("Abhijeet");
console.log(person1.getName());

var person2 = new CPerson("Subodh");
console.log(person2.getName());
person2.setName("Ramakant");
console.log(person2.getName());

console.log(person1);
console.log(person2);

// 136 bytes (68 bytes each)