var i = 10;
console.log(i);
console.log(typeof i);

var f = function () {
    console.log("Hello from function");
};
console.log(f);
console.log(typeof f);

// Function is a type, which can refer to a block of code