// function hi() {
//     console.log("Hello World!");
// }

// // Error: Duplicate function implementation.
// function hi(name: string) {
//     console.log("Hello " + name);
// }

// hi();
// hi("John");

// --------------------------------------- 
function hi(): void;
function hi(name: string): void;

function hi(...args: string[]): void {
    if(args.length === 0)
        console.log("Hello World!");
    else
        console.log("Hello " + args.join(", ") + "!");
}

hi();
hi('Synechron');
// hi("Synechron", "Technologies");             // Error: Expected 1 arguments, but got 2