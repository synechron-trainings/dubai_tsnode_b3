// Implicitly typed arrays
// var arr1 = [10, 20, 30, 40, 50];
// var arr2 = ["A", "B", "C", "D"];
// var arr3 = [10, "A", 20, "B", true];

// Explicitly typed arrays
// var arr4: number[];
// arr4 = [10, 20, 30, 40, 50];

// var arr5: Array<number>;
// arr5 = [10, 20, 30, 40, 50];

// var arr6: Array<number> = [10, 20, 30, 40, 50];

// var arr7 = new Array<number>();
// console.log(arr7);
// console.log(arr7.length);

// var arr8 = new Array<number>(5);
// console.log(arr8);
// console.log(arr8.length);

// var arr9 = new Array<string>("Manish");
// console.log(arr9);
// console.log(arr9.length);

// var arr10 = Array.of(5)
// console.log(arr10);
// console.log(arr10.length);

// var arr = [1, 2, 3, 4, 5];

// // var arr11 = new Array(arr);
// // console.log(arr11);
// // console.log(arr11.length);

// // var arr12 = Array.from(arr);
// // console.log(arr12);
// // console.log(arr12.length);

// var arr12 = [...arr];
// console.log(arr12);
// console.log(arr12.length);

// // ----------------------------------------------

// var empList: Array<{ id: number, name: string, city: string }>;

// empList = [
//     { id: 1, name: "Manish", city: "Pune" },
//     { id: 2, name: "John", city: "Mumbai" },
//     { id: 3, name: "Mike", city: "New York" },
//     { id: 4, name: "Smith", city: "London" },
//     { id: 5, name: "Scott", city: "Sydney" }
// ];

// empList.push({ id: 6, name: "Smith M", city: "London" });
// console.log(empList);

// ---------------------------------------------- Type Alias

type Employee = { id: number, name: string, city: string };

var empList: Array<Employee>;

empList = [
    { id: 1, name: "Manish", city: "Pune" },
    { id: 2, name: "John", city: "Mumbai" },
    { id: 3, name: "Mike", city: "New York" },
    { id: 4, name: "Smith", city: "London" },
    { id: 5, name: "Scott", city: "Sydney" }
];

empList.push({ id: 6, name: "Smith M", city: "London" });
// console.log(empList);

// delete empList[2];

// empList.forEach((item, index) => {
//     console.log(`Item at index ${index}: `, item);
// });

// for (let index = 0; index < empList.length; index++) {
//     console.log(`Item at index ${index}: `, empList[index]);
// }

// for (const index in empList) {
//     console.log(`Item at index ${index}: `, empList[index]);
// }

// for (const item of empList) {
//     console.log(item);
// }

// ---------------------------------------------- Array Methods
let r1 = empList.find(emp => emp.id === 3);
console.log(r1);

let r2 = empList.findIndex(emp => emp.id === 3);
console.log(r2);

let names = empList.map(emp => emp.name.toUpperCase());
console.log(names);

let ids = empList.map(emp => emp.id);
console.log(ids);

let sum =ids.reduce((acc, id) => acc + id, 0);
console.log(sum);

let cities = empList.map(emp => emp.city);
console.log(cities);

let uniqeCities = Array.from(new Set(cities));
console.log(uniqeCities);

var r3 = uniqeCities.includes("Pune");
console.log(r3);