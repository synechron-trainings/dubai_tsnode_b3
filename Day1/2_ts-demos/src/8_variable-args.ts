// ------------------------- REST PARAMETER -------------------------
// Rest parameter allows us to represent an indefinite number of arguments as an array.
// Rest parameter is similar to varargs in Java.
// Rest parameter is prefixed with three dots (...)
// Rest parameter must be the last parameter in the function definition.
// Rest parameter is used to pass a variable number of arguments to a function.

function Average(...args: number[]) {
    if (args.length > 0)
        return args.reduce((sum, n) => sum + n) / args.length;
    else
        return 0;
}

console.log(Average());
console.log(Average(1));
console.log(Average(1, 2));
console.log(Average(1, 2, 3, 4, 5));
console.log(Average(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

// Combine comma seperated items into a Array (...) - Rest Parameter
// ... used in function parameter at the time of function creation - Rest Parameter
// ... on Left hand side of assignment operator - Rest

var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// console.log(Average(numbers));          // Error: Argument of type 'number[]' is not assignable to parameter of type 'number'.
console.log(Average(...numbers));          // Spread Operator - Unpacks the array elements into individual elements

// Split (Unpack) Array/Object to a comma seperated items - Spread Operator
// ... used in function argument at the time of function call - Spread Operator
// ... on Right hand side of assignment operator - Spread Operator