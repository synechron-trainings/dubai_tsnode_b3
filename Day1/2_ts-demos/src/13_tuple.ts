// How to Store a fixed collection of values of same or varied types, maintaining the sequence

// Typeguard Array
// var dataArr1: (number | string)[];
var dataArr1: Array<number | string>;
dataArr1 = [1, "Manish"];
dataArr1 = ["Abhijeet", 2];
dataArr1 = ["Abhijeet", "Manish"];
dataArr1 = [1, 2];
dataArr1 = [1, 2, 3, 4, 5];
dataArr1 = [1, "Manish", "Pune", 411021];

// Tuple
var dataRow: [number, string];
dataRow = [1, "Manish"];
// dataRow = ["Abhijeet", 2];             // Error: Type 'string' is not assignable to type 'number'.
// dataRow = ["Abhijeet", "Manish"];      // Error: Type 'string' is not assignable to type 'number'.
// dataRow = [1, 2];                      // Error: Type 'number' is not assignable to type 'string'.
// dataRow = [1, 2, 3, 4, 5];             // Error: Type 'number' is not assignable to type 'string'.
// dataRow = [1, "Manish", "Pune", 411021];   // Error: Type '[number, string, string, number]' is not assignable to type '[number, string]'.

