// Variables created in TypeScript are optionally typesafe
// Untyped Variable - Not TypeSafe, we will not get any intellisense on an untyped variable (any)
// var data;
// data = 10;
// data = "Hello";

// Typed Variable - TypeSafe, we will get intellisense on a typed variable

// Implicit Typing
// var data = 10;
// data = "Manish";        // Error: Type 'string' is not assignable to type 'number'

// var ename = "Manish";
// ename = 10;             // Error: Type 'number' is not assignable to type 'string'

// Explicit Typing
// var age: number;
// age = 10;
// // age = "Manish";        // Error: Type 'string' is not assignable to type 'number'

// Function to add 2 numbers
// function add(x, y) {
//     return x + y;
// }

// function add(x: any, y: any) {
//     return x + y;
// }

// function add(x: number, y: number) {
//     return x + y;
// }

// console.log(add(2, 3));
// // console.log(add(2, "abc"));
// // console.log(add("abc", "xyz"));
// // console.log(add("abc", true));

// Lefthand side of assignment operator, all JS Types can be used (Declaration)
// number / string / boolean / null / undefined / void / Array / Object / RegExp / Date
// All TS types can be used (Declaration)
// any / never / unknown / Tuple / Enum / Interface / Class / Function / Type Alias / Generics
// All the new types which are supported by the latest version of ECMASCRIPT

var a: Array<number>;
var s: Symbol;
var p: Promise<number>;

// Righthand side of assignment operator, API's (Functions or Types) will come (Initialization)
// If you want to use any API, You can only use them with proper configuration
// Based on target in tsconfig.json
// And lib section configured in your tsconfig.json

// VS Code gives you intellisense using d.ts files (Type Definition Files)
// d.ts files are nothing but type definitions for the libraries

// Install Type Definition Files (TDS) using npm install -D @types/<package-name>
// d.ts files are available in node_modules/@types folder

// a = new Array<number>();
// s = Symbol("Hello");
// p = new Promise<number>((resolve, reject) => {
//     resolve(10);
// });

