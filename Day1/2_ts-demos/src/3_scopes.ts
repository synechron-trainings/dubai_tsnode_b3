// Global Scope (If module is none in tsconfig.json)
// Namespace Scope (Logical Grouping of code)
// Module Scope (If module is amd, commonjs, system, umd, es6 in tsconfig.json)
// Function Scope
// Block Scope - let / const

// var a = "Manish";

// namespace Demo { 
//     var a = "Manish";
//     console.log(a);
// }

// console.log("Outside",  a);

// namespace Demo { 
//     export var a = "Manish";
//     console.log(a);
// }

// console.log("Outside",  Demo.a);

// export var a = "Manish";

// export function display() {
//     console.log(a);
// }

// export class Employee {
//     constructor() {
//         console.log(a);
//     }
// }

// // ---------------------------- Function Scope
// // var i = 10;

// // // function test() {
// // //     var i = "Hello";
// // //     console.log("Inside Function, i is: ", i);
// // // }

// // // var doesn't have block scope
// // function test() {
// //     if (true) {
// //         var i = "Hello";
// //         console.log("Inside Block, i is: ", i);
// //     }
// //     console.log("Inside Function, i is: ", i);
// // }

// // test();
// // console.log("Outside Function, i is:", i);

// var i = 100;
// console.log("Before Loop, i is:", i);

// for (var i = 0; i < 5; i++) {
//     console.log("Inside Loop, i is:", i);
// }

// console.log("After Loop, i is:", i);

// ---------------------------- Block Scope
// // var i = 10;

// // function test() {
// //     if (true) {
// //         let i = "Hello";
// //         console.log("Inside Block, i is: ", i);
// //     }
// //     console.log("Inside Function, i is: ", i);
// // }

// // test();
// // console.log("Outside Function, i is:", i);

// var i = 100;
// console.log("Before Loop, i is:", i);

// for (let i = 0; i < 5; i++) {
//     console.log("Inside Loop, i is:", i);
// }

// console.log("After Loop, i is:", i);

// ---------------------------- Block Scope (Const)
let j: number;
j = 10;
j = 20;

// const env: string;              // Error: Const declarations must be initialized

const env: string = "Development";
console.log("Outside, env is:", env);

// env = "Production";            // Error: Cannot assign to 'env' because it is a constant
// console.log("Outside, env is:", env);

if (true) {
    // env = "Production";            // Error: Cannot assign to 'env' because it is a constant
    // console.log("Inside, env is:", env);

    const env = "Production";
    console.log("Inside, env is:", env);
}