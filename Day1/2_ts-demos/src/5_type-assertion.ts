let data1: any = "Hello, I am a string";
// console.log(data1.toUppercase());
// console.log(data1.toUpperCase());

// Type Assertion
// 1. Angle Bracket Syntax
console.log((<string>data1).toUpperCase());
// 2. as Syntax
console.log((data1 as string).toUpperCase());

// Wrong Assertion will cost you runtime error
let r = (data1 as number).toFixed(2);
console.log(r);
