// // function Open(mode: string) {
// //     console.log(`Opening the file in "${mode}" mode`);
// // }

// function Open(mode: string) {
//     switch(mode) {
//         case "Read":
//         case "Write":
//         case "Append":
//         case "ReadAndWrite":
//             console.log(`Opening the file in "${mode}" mode`);
//             break;
//         default:
//             console.log(`Invalid mode "${mode}"`);
//             break;
//     }
// }

// Open("Read");
// Open("Write");
// Open("Append");
// Open("ReadAndWrite");
// Open("Abhay");
// Open("Ahmad");
// Open("Ashish");

// --------------------------------- Enum
// Enums allow you to define a set of constraints on the values that can be used in your code.

// enum FileMode { Read, Write, Append, ReadAndWrite }
// enum FileMode { Read = 10, Write = 20, Append = 30, ReadAndWrite }
enum FileMode { Read = "Read", Write = "Write", Append = "Append", ReadAndWrite = "ReadAndWrite" }
// enum FileMode { Read = 10, Write = 20, Append = "Append", ReadAndWrite = "ReadAndWrite" }

function Open(mode: FileMode) {
    console.log(`Opening the file in "${mode}" mode`);
}

Open(FileMode.Read);
Open(FileMode.Write);
Open(FileMode.Append);
Open(FileMode.ReadAndWrite);