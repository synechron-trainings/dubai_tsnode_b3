function hello() {
    console.log('Hello World');
}

// var r1 = hello();
// console.log(r1); // undefined
// console.log(typeof r1); // undefined

// var r1: undefined;
// // r1 = 10; // Error: Type '10' is not assignable to type 'undefined'
// r1 = undefined;
// r1 = null;
// r1 = void 0;
// console.log(r1);

// var r1: void;
// // r1 = 10; // Error: Type '10' is not assignable to type 'void'
// r1 = undefined;
// r1 = null;
// r1 = void 0;
// console.log(r1);

// var r1: null;
// // r1 = 10; // Error: Type '10' is not assignable to type 'null'
// r1 = undefined;
// r1 = null;
// r1 = void 0;
// console.log(r1);

// var r1: never;
// // r1 = 10;    // Error: Type '10' is not assignable to type 'never'
// // r1 = undefined; // Error: Type 'undefined' is not assignable to type 'never'
// // r1 = null; // Error: Type 'null' is not assignable to type 'never'
// // r1 = void 0; // Error: Type 'void' is not assignable to type 'never'
// console.log(r1);

// var r1: any;
// console.log(r1.trim());         // No Compilation Error, but Runtime Error may occur

// var r1: unknown;
// console.log(r1.trim());         // Error: Property 'trim' does not exist on type 'unknown'

// ---------------------------------------------------------------------

// function iterate() {
//     let i = 1;

//     while (true) {
//         console.log(i++);
//     }
// }

// function iterate(): never {
//     let i = 1;

//     while (true) {
//         console.log(i++);
//     }
// }

// iterate(); // Infinite Loop
// console.log("Last Line of Code");

// ---------------------------------------------------------

// Function Declaration
function add1(x: number, y: number): number {
    return x + y;
}

// Function Expression
let add2 = function (x: number, y: number): number {
    return x + y;
}

let add3: (x: number, y: number) => number;
add3 = function (x: number, y: number): number {
    return x + y;
}

let add4: (x: number, y: number) => number = function (x: number, y: number): number {
    return x + y;
}

let add5: (x: number, y: number) => number;
add5 = function (x, y) {
    return x + y;
}

// Multi-line Lambda
let add6: (x: number, y: number) => number;
add6 = (x, y) => {
    return x + y;
}

// Single-line Lambda
let add7: (x: number, y: number) => number;
add7 = (x, y) => x + y;

console.log(add1(2, 3));
console.log(add2(2, 3));
console.log(add3(2, 3));
console.log(add4(2, 3));
console.log(add5(2, 3));
console.log(add6(2, 3));
console.log(add7(2, 3));