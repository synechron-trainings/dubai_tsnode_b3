function hello_js(name) {
    console.log(`Hello ${name}`);
}

hello_js('Synechron');
hello_js(10);
hello_js('Synechron', 'Bangalore');
hello_js();