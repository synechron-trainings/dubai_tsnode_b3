import { Server, createServer } from 'http';
import express, { Request, Response } from 'express';
import favicon from 'serve-favicon';
import path from 'path';
import logger from 'morgan';

const app = express();

app.set('view engine', 'pug');

const employees = [
    { id: 1, name: "Manish" },
    { id: 2, name: "Abhijeet" },
    { id: 3, name: "Ram" },
    { id: 4, name: "Abhishek" },
    { id: 5, name: "Ramakant" }
];

app.use(logger('dev'));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

// app.use((request: Request, response: Response, next: Function) => {
//     console.log("Request - Middleware One");
//     next();
//     console.log("Response - Middleware One");
// });

// app.use((request: Request, response: Response, next: Function) => {
//     console.log("Request - Middleware Two");
//     next();
//     console.log("Response - Middleware Two");
// });

// app.use((request: Request, response: Response, next: Function) => {
//     const startTime = new Date().getTime();
//     next();
//     response.on('finish', () => {
//         const endTime = new Date().getTime();
//         console.log(`Request URL: ${request.url} - Total Time: ${endTime - startTime} ms`);
//     });
// });

app.get('/', (request: Request, response: Response) => {
    console.log("Request Handler Executed");
    // throw new Error('Something went wrong');
    response.render('home/index', { pageTitle: 'Home Page' });
});

app.get('/about', (request: Request, response: Response) => {
    response.render('home/about', { pageTitle: 'About Page' });
});

app.get('/contact', (request: Request, response: Response) => {
    response.render('home/contact', { pageTitle: 'Contact Page' });
});

app.get('/employees', (request: Request, response: Response) => {
    response.render('employees/index', { pageTitle: 'Employees Page', empList: employees });
});

app.use((error: Error, request: Request, response: Response, next: Function) => {
    response.locals.message = error.message;
    response.locals.error = request.app.get('env') === 'development' ? error : {};
    response.status(500);
    response.render('error', { pageTitle: 'Error Page' });
});

// ---------------------- Hosting Code
const server: Server = createServer(app);

server.listen(3000);

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Server is listening on port ${port}`);
}

function onError(error: Error) {
    console.error(`Error occurred: ${error.message}`);
}

server.on('listening', onListening);
server.on('error', onError);


// ----------------------------------------- Only using http module -----------------------------------------
// import { Server, createServer, IncomingMessage, ServerResponse } from 'http';
// import * as fs from 'fs';
// import * as path from 'path';

// const employees = [
//     { id: 1, name: "Manish" },
//     { id: 2, name: "Abhijeet" },
//     { id: 3, name: "Ram" },
//     { id: 4, name: "Abhishek" },
//     { id: 5, name: "Ramakant" }
// ];

// const requestHandler = (request: IncomingMessage, response: ServerResponse) => {
//     if (request.url === '/') {
//         const indexPath = path.join(process.cwd(), 'public', 'index.html');

//         fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
//             if (error) {
//                 response.statusCode = 500;
//                 response.end();
//                 return;
//             }

//             response.setHeader('content-type', 'text/html');
//             response.write(data);
//             response.end();
//         });
//     } else if (request.url === '/about') {
//         const indexPath = path.join(process.cwd(), 'public', 'about.html');

//         fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
//             if (error) {
//                 response.statusCode = 500;
//                 response.end();
//                 return;
//             }

//             response.setHeader('content-type', 'text/html');
//             response.write(data);
//             response.end();
//         });
//     } else if (request.url === '/contact') {
//         const indexPath = path.join(process.cwd(), 'public', 'contact.html');

//         fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
//             if (error) {
//                 response.statusCode = 500;
//                 response.end();
//                 return;
//             }

//             response.setHeader('content-type', 'text/html');
//             response.write(data);
//             response.end();
//         });
//     } else if (request.url === '/data') {
//         const data = JSON.stringify(employees);
//         response.setHeader('content-type', 'application/json');
//         response.write(data);
//         response.end();
//     } else {
//         response.statusCode = 404;
//         response.end();
//     }
// }

// const server: Server = createServer(requestHandler);

// server.listen(3000);

// function onListening() {
//     const address = server.address();
//     const port = typeof address === 'string' ? address : address?.port;
//     console.log(`Server is listening on port ${port}`);
// }

// function onError(error: Error) {
//     console.error(`Error occurred: ${error.message}`);
// }

// server.on('listening', onListening);
// server.on('error', onError);