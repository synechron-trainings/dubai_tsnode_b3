import axios, { AxiosResponse } from 'axios';

import * as path from 'path';
import * as fs from 'fs';
import { Post } from './models/post';

const url: string = 'https://jsonplaceholder.typicode.com/posts';

const writeFilePath = path.join(process.cwd(), 'files', 'posts.json');

(async function(){
    try {
        const response: AxiosResponse<Array<Post>> = await axios.get<Array<Post>>(url);
        const posts: Post[] = response.data;

        console.log('All data received and parsed....');
        fs.writeFileSync(writeFilePath, JSON.stringify(posts, null, 2));

        console.log("\nFirst Record:");
        console.log("Id: ", posts[0].id);
        console.log("Title: ", posts[0].title);
        console.log("Body: ", posts[0].body);
    } catch(error) {
        console.error(error);
    }
})();