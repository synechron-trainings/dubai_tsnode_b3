import { EventEmitter } from "events";

class StringEmitter extends EventEmitter {
    private static instance: StringEmitter;

    // Make the constructor private to prevent direct construction calls with the `new` operator
    private constructor() {
        super();
        this.run();
    }

    // Provide a static method to control the access to the singleton instance
    public static getInstance(): StringEmitter {
        if (!StringEmitter.instance) {
            StringEmitter.instance = new StringEmitter();
        }
        return StringEmitter.instance;
    }

    public run(): void {
        const strArr = ["NodeJS", "ReactJS", "Angular", "ExtJS", "jQuery"];
        setInterval(() => {
            const str = strArr[Math.floor(Math.random() * strArr.length)];
            this.emit("data", str);
        }, 2000);
    }
}

export const stringEmitter = StringEmitter.getInstance();