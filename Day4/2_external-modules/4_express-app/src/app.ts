import { Server, createServer } from 'http';
import express, { Request, Response } from 'express';
import * as path from 'path';

const app = express();

const employees = [
    { id: 1, name: "Manish" },
    { id: 2, name: "Abhijeet" },
    { id: 3, name: "Ram" },
    { id: 4, name: "Abhishek" },
    { id: 5, name: "Ramakant" }
];

app.get('/', (request: Request, response: Response) => {
    response.sendFile(path.join(process.cwd(), 'public', 'index.html'));
});

app.get('/about', (request: Request, response: Response) => {
    response.sendFile(path.join(process.cwd(), 'public', 'about.html'));
});

app.get('/contact', (request: Request, response: Response) => {
    response.sendFile(path.join(process.cwd(), 'public', 'contact.html'));
});

app.get('/data', (request: Request, response: Response) => {
    response.json(employees);
});

// ---------------------- Hosting Code
const server: Server = createServer(app);

server.listen(3000);

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Server is listening on port ${port}`);
}

function onError(error: Error) {
    console.error(`Error occurred: ${error.message}`);
}

server.on('listening', onListening);
server.on('error', onError);


// ----------------------------------------- Only using http module -----------------------------------------
// import { Server, createServer, IncomingMessage, ServerResponse } from 'http';
// import * as fs from 'fs';
// import * as path from 'path';

// const employees = [
//     { id: 1, name: "Manish" },
//     { id: 2, name: "Abhijeet" },
//     { id: 3, name: "Ram" },
//     { id: 4, name: "Abhishek" },
//     { id: 5, name: "Ramakant" }
// ];

// const requestHandler = (request: IncomingMessage, response: ServerResponse) => {
//     if (request.url === '/') {
//         const indexPath = path.join(process.cwd(), 'public', 'index.html');

//         fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
//             if (error) {
//                 response.statusCode = 500;
//                 response.end();
//                 return;
//             }

//             response.setHeader('content-type', 'text/html');
//             response.write(data);
//             response.end();
//         });
//     } else if (request.url === '/about') {
//         const indexPath = path.join(process.cwd(), 'public', 'about.html');

//         fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
//             if (error) {
//                 response.statusCode = 500;
//                 response.end();
//                 return;
//             }

//             response.setHeader('content-type', 'text/html');
//             response.write(data);
//             response.end();
//         });
//     } else if (request.url === '/contact') {
//         const indexPath = path.join(process.cwd(), 'public', 'contact.html');

//         fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
//             if (error) {
//                 response.statusCode = 500;
//                 response.end();
//                 return;
//             }

//             response.setHeader('content-type', 'text/html');
//             response.write(data);
//             response.end();
//         });
//     } else if (request.url === '/data') {
//         const data = JSON.stringify(employees);
//         response.setHeader('content-type', 'application/json');
//         response.write(data);
//         response.end();
//     } else {
//         response.statusCode = 404;
//         response.end();
//     }
// }

// const server: Server = createServer(requestHandler);

// server.listen(3000);

// function onListening() {
//     const address = server.address();
//     const port = typeof address === 'string' ? address : address?.port;
//     console.log(`Server is listening on port ${port}`);
// }

// function onError(error: Error) {
//     console.error(`Error occurred: ${error.message}`);
// }

// server.on('listening', onListening);
// server.on('error', onError);