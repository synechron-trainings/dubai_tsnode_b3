import express from 'express';
import * as employeesController from '../controllers/employees-controller';

const router = express.Router();

router.get('/', employeesController.renderIndex);

router.get('/details/:empid', employeesController.renderDetails);

router.get('/create', employeesController.renderCreateEmployee);

router.get('/edit/:empid', employeesController.renderEditEmployee);

router.get('/delete/:empid', employeesController.renderDeleteEmployee);

export default router;