import { Server, createServer } from 'http';
import app from '../app';

const server: Server = createServer(app);

server.listen(process.env.PORT || 3000);

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Server is listening on port ${port}`);
}

function onError(error: Error) {
    console.error(`Error occurred: ${error.message}`);
}

server.on('listening', onListening);
server.on('error', onError);