import { Request, Response } from 'express';
import { getEmployee, getEmployees } from '../data-access';

export const renderIndex = (request: Request, response: Response) => {
    response.render('employees/index', { pageTitle: 'Employees Page', empList: getEmployees() });
}

export const renderDetails = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);
    response.render('employees/details', { pageTitle: 'Employee Details Page', employee: getEmployee(id) });
}

export const renderCreateEmployee = (request: Request, response: Response) => {
    response.render('employees/create', { pageTitle: 'Create Employee Page', employee: {}  });
}

export const renderEditEmployee = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);
    response.render('employees/edit', { pageTitle: 'Edit Employee Page', employee: getEmployee(id) });
}

export const renderDeleteEmployee = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);
    response.render('employees/delete', { pageTitle: 'Delete Employee Page', employee: getEmployee(id) });
}