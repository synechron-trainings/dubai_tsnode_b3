// Association
// Association is a relationship where all objects have their own lifecycle and there is no owner.

// Aggregation
// Aggregation a weaker relationship where the parts can exist independently of the whole

// Composition
// The lifecycle of the contained objects depends on the lifecycle of the container object. 

// Aggregation
class Project {
    manager: Employee;

    constructor(emp: Employee) {
        this.manager = emp;
    }

    setManager(emp: Employee) {
        this.manager = emp;
    }

    set Manager(emp: Employee) { 
        this.manager = emp;
    }
}

// Composition
class Employee {
    idCard: Identity;

    constructor() {
        this.idCard = new Identity(1234);
    }
}

class Identity {
    idNumber: number;

    constructor(idNumber: number) {
        this.idNumber = idNumber;
    }
}

let emp = new Employee();

let project = new Project(emp);         // Dependency Injection (Constructor Injection)
// project = null;
// console.log(project.manager.idCard.idNumber);

// console.log(emp.idCard.idNumber);

let emp2 = new Employee();

// project.setManager(emp2);               // Dependency Injection (Method Injection)
project.Manager = emp2;                    // Dependency Injection (Property Injection)