import { AccountFactory } from "./account";
// import { registerNotificationServices } from "./config/notification-service-registry-config";
import { NotificationHandlerFactory } from "./utilities/notification-handler";
import "./config/notification-service-registry-config";

// registerNotificationServices();
const notificationHandler = NotificationHandlerFactory.createHandler();
const a1 = AccountFactory.createAccount(1, 1000, 0.05, notificationHandler);

a1.deposit(1000);
a1.withdraw(2000);
a1.withdraw(1000);

const a2 = AccountFactory.createAccount(1, 1000, 0.05, notificationHandler);

a2.deposit(500);
a2.withdraw(1500);
a2.withdraw(500);