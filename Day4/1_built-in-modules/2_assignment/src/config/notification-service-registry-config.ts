// import { whatsAppService } from "../utilities/whatsapp";
// import { consoleService } from "../utilities/console";
// import { emailService } from "../utilities/email";
// import { NotificationServiceRegistry } from "../utilities/notification-service-registry";
// import { smsService } from "../utilities/sms";

// export function registerNotificationServices() {
//     NotificationServiceRegistry.registerService(consoleService);
//     NotificationServiceRegistry.registerService(smsService);
//     NotificationServiceRegistry.registerService(emailService);
//     NotificationServiceRegistry.registerService(whatsAppService);
// }

import { whatsAppService } from "../utilities/whatsapp";
import { consoleService } from "../utilities/console";
import { emailService } from "../utilities/email";
import { NotificationServiceRegistry } from "../utilities/notification-service-registry";
import { smsService } from "../utilities/sms";

(function registerNotificationServices() {
    NotificationServiceRegistry.registerService(consoleService);
    NotificationServiceRegistry.registerService(smsService);
    NotificationServiceRegistry.registerService(emailService);
    NotificationServiceRegistry.registerService(whatsAppService);
})();