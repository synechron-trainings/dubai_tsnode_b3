import { NotificationService } from "./notification-service";

class SMS implements NotificationService {
    private static instance: SMS;

    private constructor() { }

    static getInstance(): SMS {
        if (!SMS.instance) {
            SMS.instance = new SMS();
        }
        return SMS.instance;
    }

    send(message: string) {
        console.log("SMS Sent - ", message);
    }
}

export const smsService = SMS.getInstance();