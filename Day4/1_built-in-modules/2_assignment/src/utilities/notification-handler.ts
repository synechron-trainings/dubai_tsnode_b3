import { Account } from '../account';
import { NotificationService } from './notification-service';
import { NotificationServiceRegistry } from './notification-service-registry';

export class NotificationHandler {
    constructor(private notificationServices: NotificationService[]) { }

    listen(account: Account) { 
        account.on('depositSuccess', (balance) => {
            this.notify(`Deposit successful. Balance is: ${balance}`);
        });
        
        account.on('withdrawSuccess', (balance) => {
            this.notify(`Withdraw successful. Balance is: ${balance}`);
        });
        
        account.on('withdrawFailure', (balance) => {
            this.notify(`Withdraw failed. Insufficient balance. Balance is: ${balance}`);
        });
    }

    private notify(message: string) {
        this.notificationServices.forEach((service) => {
            service.send(message);
        });
    }
}

export class NotificationHandlerFactory {
    static createHandler(): NotificationHandler {
        const notificationServices = NotificationServiceRegistry.getRegisteredServices();
        return new NotificationHandler(notificationServices);
    }
}