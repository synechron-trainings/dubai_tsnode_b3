import { NotificationService } from "./notification-service";

class Email implements NotificationService {
    private static instance: Email;

    private constructor() { }

    static getInstance(): Email {
        if (!Email.instance) {
            Email.instance = new Email();
        }
        return Email.instance;
    }

    send(message: string) {
        console.log("Email Sent - ", message);
    }
}

export const emailService = Email.getInstance();