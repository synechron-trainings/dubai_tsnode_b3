import { NotificationService } from "./notification-service";

class WhatsApp implements NotificationService {
    private static instance: WhatsApp;

    private constructor() { }

    static getInstance(): WhatsApp {
        if (!WhatsApp.instance) {
            WhatsApp.instance = new WhatsApp();
        }
        return WhatsApp.instance;
    }

    send(message: string) {
        console.log("WhatsApp Sent - ", message);
    }
}

export const whatsAppService = WhatsApp.getInstance();