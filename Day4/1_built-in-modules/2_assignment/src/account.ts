import EventEmitter from "events";
import { NotificationHandler } from "./utilities/notification-handler";

export class Account extends EventEmitter {
    private _accNumber: number;
    private _balance: number;
    private _annualIntrestRate: number;

    constructor(accNumber = 0, balance = 0, intRate = 0) {
        super();
        this._accNumber = accNumber;
        this._balance = balance;
        this._annualIntrestRate = intRate;
    }

    get AccountNumber() {
        return this._accNumber;
    }

    get AnnualInterestRate() {
        return this._annualIntrestRate;
    }

    set Balance(balance: number) {
        this._balance = balance;
    }

    set AnnualInterestRate(annualInterestRate: number) {
        this._annualIntrestRate = annualInterestRate;
    }

    deposit(amount: number) {
        this._balance += amount;
        this.emit('depositSuccess', this._balance);
    }

    withdraw(amount: number) {
        if (this._balance >= amount) {
            this._balance -= amount;
            this.emit('withdrawSuccess', this._balance);
        } else {
            this.emit('withdrawFailure', this._balance);
        }
    }

    toString() {
        return `Account: ${this._accNumber}, Balance is: ${this._balance}`;
    }
}

export class AccountFactory {
    static createAccount(accNumber: number, balance: number, intRate: number, notificationHandler: NotificationHandler) {
        const account = new Account(accNumber, balance, intRate);
        if (notificationHandler) {
            notificationHandler.listen(account);
        }
        return account;
    }
}