import { Server, createServer, IncomingMessage, ServerResponse } from 'http';
import * as fs from 'fs';
import * as path from 'path';

const requestHandler = (request: IncomingMessage, response: ServerResponse) => {
    if (request.url === '/') {
        const indexPath = path.join(process.cwd(), 'public', 'index.html');

        fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
            if (error) {
                response.statusCode = 500;
                response.end();
                return;
            }

            response.setHeader('content-type', 'text/html');
            response.write(data);
            response.end();
        });
    } else if (request.url?.startsWith('/download')) {
        const fileNumber = request.url.split('/')[2];
        const filePath = path.join(process.cwd(), 'files', `${fileNumber}.pdf`);
        serveFile(filePath, 'application/pdf', response, false);
        // serveFile(filePath, 'application/pdf', response, true);
    }
    else {
        response.statusCode = 404;
        response.end();
    }
}

function serveFile(filePath: string, contentType: string, response: ServerResponse, isAttachment: boolean) {
    const fileName = path.basename(filePath);
    const readStream = fs.createReadStream(filePath);

    readStream.on('open', () => {
        response.setHeader('content-type', contentType);
        if (isAttachment) {
            response.setHeader('content-disposition', `attachment; filename=${fileName}`);
        }
        response.statusCode = 200;
        readStream.pipe(response);
    });

    readStream.on('error', () => {
        response.setHeader('content-type', 'text/plain');
        response.statusCode = 404;
        response.write('File not found');
        response.end();
    });
}

const server: Server = createServer(requestHandler);

server.listen(3000);

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Server is listening on port ${port}`);
}

function onError(error: Error) {
    console.error(`Error occurred: ${error.message}`);
}

server.on('listening', onListening);
server.on('error', onError);