Problem Statement: Build a Node.js Server for PDF Downloads

Objective:
Develop a Node.js application using Express that allows users to download PDF files. 
The application will serve an HTML page that includes links to download specific PDF files located in a server-side files directory.