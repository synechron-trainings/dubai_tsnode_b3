// import * as https from 'https';
// import { ClientRequest, IncomingMessage } from 'http';
// import * as path from 'path';
// import * as fs from 'fs';

// const url: string = 'https://jsonplaceholder.typicode.com/posts';

// const writeFilePath = path.join(process.cwd(), 'files', 'posts.json');
// const writeStream = fs.createWriteStream(writeFilePath, { encoding: 'utf-8' });

// const requestOptions: https.RequestOptions = {
//     method: 'GET',
//     headers: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json'
//     }
// };

// const request: ClientRequest = https.request(url, requestOptions, (response: IncomingMessage) => {
//     if (response.statusCode !== 200) {
//         console.error(`Error occurred while fetching data. Status code: ${response.statusCode}`);
//         return;
//     }

//     response.on('data', (chunk: Buffer) => {
//         // console.log("Data Received: ", chunk.length, "bytes");
//         writeStream.write(chunk);
//     });

//     response.on('end', () => {
//         console.log("Data Received Successfully");
//         writeStream.end();
//     });
// });

// request.end();

// ---------------------------- Mapping Response to TypeScript Object ----------------------------

import * as https from 'https';
import { ClientRequest, IncomingMessage } from 'http';
import * as path from 'path';
import * as fs from 'fs';
import { Post } from './models/post';

const url: string = 'https://jsonplaceholder.typicode.com/posts';

const writeFilePath = path.join(process.cwd(), 'files', 'posts1.json');

const requestOptions: https.RequestOptions = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
};

const request: ClientRequest = https.request(url, requestOptions, (response: IncomingMessage) => {
    if (response.statusCode !== 200) {
        console.error(`Error occurred while fetching data. Status code: ${response.statusCode}`);
        return;
    }

    let dataChunks: Buffer[] = [];

    response.on('data', (chunk: Buffer) => {
        dataChunks.push(chunk);
    });

    response.on('end', () => {
        const data = Buffer.concat(dataChunks).toString();
        const posts: Post[] = JSON.parse(data);
        
        console.log("Data Received Successfully");
        fs.writeFileSync(writeFilePath, JSON.stringify(posts, null, 2));

        console.log("Data Written Successfully");
        console.log("\nFirst Post: ");
        console.log("Id: ", posts[0].id);
        console.log("Title: ", posts[0].title);
        console.log("Body: ", posts[0].body);
    });
});

request.end();