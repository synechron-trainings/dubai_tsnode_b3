// import { Server, createServer, IncomingMessage, ServerResponse } from 'http';

// const requestHandler = (request: IncomingMessage, response: ServerResponse) => { 
//     // console.log(request.url);
//     // console.log(request.headers);
//     // response.write('Hello World');
//     // response.end();

//     response.setHeader('content-type', 'text/html');
//     // response.setHeader('content-type', 'text/plain');
//     // response.setHeader('content-type', 'application/json');
//     // response.setHeader('content-type', 'application/pdf');

//     response.write('<h1>Hello World</h1>');
//     response.end();
// }

// const server: Server = createServer(requestHandler);

// server.listen(3000);

// function onListening() {
//     const address = server.address();
//     const port = typeof address === 'string' ? address : address?.port;
//     console.log(`Server is listening on port ${port}`);
// }

// function onError(error: Error) {
//     console.error(`Error occurred: ${error.message}`);
// }

// server.on('listening', onListening);
// server.on('error', onError);

// -----------------------------------------------------------

import { Server, createServer, IncomingMessage, ServerResponse } from 'http';
import * as fs from 'fs';
import * as path from 'path';

const requestHandler = (request: IncomingMessage, response: ServerResponse) => {
    if (request.url === '/') {
        const indexPath = path.join(process.cwd(), 'public', 'index.html');
        
        fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
            if (error) {
                response.statusCode = 500;
                response.end();
                return;
            }

            response.setHeader('content-type', 'text/html');
            response.write(data);
            response.end();
        });
    } else {
        response.statusCode = 404;
        response.end();
    }
}

const server: Server = createServer(requestHandler);

server.listen(3000);

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Server is listening on port ${port}`);
}

function onError(error: Error) {
    console.error(`Error occurred: ${error.message}`);
}

server.on('listening', onListening);
server.on('error', onError);