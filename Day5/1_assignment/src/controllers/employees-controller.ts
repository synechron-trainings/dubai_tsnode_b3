import { Request, Response } from 'express';
import { findAndDeleteEmployee, findAndUpdateEmployee, getEmployee, getEmployees, insertEmployee } from '../data-access';
import { Employee } from '../models/employee';

export const renderIndex = (request: Request, response: Response) => {
    getEmployees().then(employees => {
        response.render('employees/index', { pageTitle: 'Employees Page', empList: employees, message: '' });
    }).catch(errMsg => {
        response.render('employees/index', { pageTitle: 'Employees Page', empList: [], message: errMsg });
    });
}

export const renderDetails = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);

    getEmployee(id).then(employee => {
        response.render('employees/details', { pageTitle: 'Employee Details Page', employee, message: '' });
    }).catch(errMsg => {
        response.render('employees/details', { pageTitle: 'Employee Details Page', employee: {}, message: errMsg });
    });
}

export const renderCreateEmployee = (request: Request, response: Response) => {
    response.render('employees/create', { pageTitle: 'Create Employee Page', employee: {} });
}

export const createEmployee = (request: Request, response: Response) => {
    const { eid, ename } = request.body;
    const employee = new Employee(parseInt(eid), ename);

    insertEmployee(employee).then(() => {
        response.redirect('/employees');
    }).catch(errMsg => {
        response.render('employees/create', { pageTitle: 'Create Employee Page', employee, message: errMsg });
    });
}

export const renderEditEmployee = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);

    getEmployee(id).then(employee => {
        response.render('employees/edit', { pageTitle: 'Edit Employee Page', employee, message: '' });
    }).catch(errMsg => {
        response.render('employees/edit', { pageTitle: 'Edit Employee Page', employee: {}, message: errMsg });
    });
}

export const updateEmployee = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);

    const { eid, ename } = request.body;
    const employee = new Employee(parseInt(eid), ename);

    findAndUpdateEmployee(id, employee).then(() => {
        response.redirect('/employees');
    }).catch(errMsg => {
        response.render('employees/edit', { pageTitle: 'Edit Employee Page', employee, message: errMsg });
    });
}

export const renderDeleteEmployee = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);

    getEmployee(id).then(employee => {
        response.render('employees/delete', { pageTitle: 'Delete Employee Page', employee, message: '' });
    }).catch(errMsg => {
        response.render('employees/delete', { pageTitle: 'Delete Employee Page', employee: {}, message: errMsg });
    });
}

export const deleteEmployee = (request: Request, response: Response) => {
    const id = parseInt(request.params.empid);

    findAndDeleteEmployee(id).then(() => {
        response.redirect('/employees');
    }).catch(errMsg => {
        response.render('employees/delete', { pageTitle: 'Delete Employee Page', employee: {}, message: errMsg });
    });
}