import { readData, writeData } from "../utilities/file-handler";
import { Employee } from "../models/employee";

let employees: Array<Employee> = [];

(async function initializeEmployees() {
    try {
        const data = await readData();
        employees = data.map((emp) => new Employee(emp.id, emp.name));
    } catch (err) {
        console.log(err);
        employees = [];
    }
})();

export const getEmployees = (): Promise<Array<Employee>> => {
    return new Promise((resolve, reject) => {
        if (employees.length > 0) {
            resolve(employees);
        } else {
            reject('No employees found');
        }
    });
}

export const getEmployee = (id: number): Promise<Employee> => {
    return new Promise((resolve, reject) => {
        const employee = employees.find((emp) => emp.id === id);
        if (employee)
            resolve(employee);
        else
            reject('Employee not found');
    });
}

export const insertEmployee = (employeeToInsert: Employee): Promise<Employee> => {
    return new Promise(async (resolve, reject) => {
        employees.push(employeeToInsert);
        try {
            const data = await writeData(employees);
            employees = data.map((emp) => new Employee(emp.id, emp.name));
            resolve(employees.find((emp) => emp.id === employeeToInsert.id));
        } catch (err) {
            reject(err);
        }
    });
}

export const findAndUpdateEmployee = (id: number, employeeToUpdate: Employee): Promise<Employee> => {
    return new Promise(async (resolve, reject) => {
        const index = employees.findIndex((emp) => emp.id === id);
        if (index !== -1) {
            employees[index] = { ...employees[index], ...employeeToUpdate };
            try {
                const data = await writeData(employees);
                employees = data.map((emp) => new Employee(emp.id, emp.name));
                resolve(employees.find((emp) => emp.id === id));
            } catch (err) {
                reject(err);
            }
        } else {
            reject('Employee not found');
        }
    });
}

export const findAndDeleteEmployee = (id: number): Promise<void> => {
    return new Promise(async (resolve, reject) => {
        employees = employees.filter((emp) => emp.id !== id);
        try {
            await writeData(employees);
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}
