import express from 'express';
import * as employeesController from '../controllers/employees-controller';

const router = express.Router();

router.get('/', employeesController.getAllEmployees);

router.get('/:empid', employeesController.getEmployeeDetails);

router.post('/', employeesController.createEmployee);

router.put('/:empid', employeesController.updateEmployee);

router.delete('/:empid', employeesController.deleteEmployee);

export default router;