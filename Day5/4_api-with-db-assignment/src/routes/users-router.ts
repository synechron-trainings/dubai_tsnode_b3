import express from 'express';
import * as usersController from '../controllers/users-controller';

const router = express.Router();

router.get('/', usersController.getAllUsers);

export default router;