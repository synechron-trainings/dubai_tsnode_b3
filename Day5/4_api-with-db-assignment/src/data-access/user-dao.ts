import { UserEntity } from "../entities";
import { AppDataSource } from "../config/data-source";
import { User } from "../models";

const userRepository = AppDataSource.getRepository(UserEntity);

const getUsers = async (): Promise<Array<User>> => {
    const userEntities = await userRepository.find();
    return userEntities.map((userEntity) => new User(userEntity.userId, userEntity.username, userEntity.email));
}

export const userDAO = {
    getUsers
}