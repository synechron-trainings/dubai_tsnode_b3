export class User {
    private userId: number;
    private username: string;
    private email: string;
    
    constructor(userId: number, username: string, email: string) {
        this.userId = userId;
        this.username = username;
        this.email = email;
    }

    get UserId() {
        return this.userId;
    }

    set UserId(value: number) {
        this.userId = value;
    }

    get Username() {
        return this.username;
    }

    set Username(value: string) {
        this.username = value;
    }

    get Email() {
        return this.email;
    }

    set Email(value: string) {
        this.email = value;
    }
}