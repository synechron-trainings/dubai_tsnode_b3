import { Request, Response } from 'express';
import { employeeDAO } from '../data-access';
import { Employee } from '../models/employee';

export const getAllEmployees = async (request: Request, response: Response) => {
    try {
        const employees = await employeeDAO.getEmployees();
        response.status(200).json({ employees });
    } catch (error) {
        response.status(500).json({ message: 'Failed to get employees', error });
    }
}

export const getEmployeeDetails = async (request: Request, response: Response) => {
    try {
        const id = parseInt(request.params.empid);
        const employee = await employeeDAO.getEmployee(id);

        if (employee) {
            response.status(200).json({ employee });
        } else {
            response.status(404).json({ message: 'Employee not found' });
        }
    } catch (error) {
        response.status(500).json({ message: 'Failed to get employee details', error });
    }
}

export const createEmployee = async (request: Request, response: Response) => {
    try {
        const { eid, ename } = request.body;
        const employee = new Employee(parseInt(eid), ename);
        const insertedEmployee = await employeeDAO.insertEmployee(employee);
        response.status(201).json({ message: 'Employee created successfully', employee: insertedEmployee });
    } catch (error) {
        response.status(500).json({ message: 'Failed to create employee', error });
    }
}

export const updateEmployee = async (request: Request, response: Response) => {
    try {
        const id = parseInt(request.params.empid);
        const { eid, ename } = request.body;
        const employee = new Employee(parseInt(eid), ename);
        const updatedEmployee = await employeeDAO.findAndUpdateEmployee(id, employee);
        response.status(200).json({ message: 'Employee updated successfully', employee: updatedEmployee });
    }
    catch (error) {
        response.status(500).json({ message: 'Failed to update employee', error });
    }
}

export const deleteEmployee = async (request: Request, response: Response) => {
    try {
        const id = parseInt(request.params.empid);
        await employeeDAO.findAndDeleteEmployee(id);
        response.status(204).end();
        // response.status(200).json({ message: 'Employee deleted successfully' });
    } catch (error) {
        response.status(500).json({ message: 'Failed to delete employee', error });
    }
}