import { Request, Response } from 'express';
import { userDAO } from "../data-access";

export const getAllUsers = async (request: Request, response: Response) => {
    try {
        const users = await userDAO.getUsers();
        response.status(200).json({ users });
    } catch (error) {
        response.status(500).json({ message: 'Failed to get users', error });
    }
}