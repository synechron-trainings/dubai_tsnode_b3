import swaggerJSDoc from 'swagger-jsdoc';
import path from 'path';

const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Synechron ENBD API",
            description: "API documentation for Synechron ENBD",
            version: "1.0.0",
            contact: {
                name: "Synechron ENBD",
                email: "synechron@enbd.com"
            },
            license: {
                name: "MIT",
                url: "https://opensource.org/licenses/MIT"
            }
        }
    },
    apis: [path.resolve(__dirname, '../routes/*.ts')]
};

const swaggerSpec = swaggerJSDoc(options);
export default swaggerSpec;