// import express from 'express';
// import * as employeesController from '../controllers/employees-controller';

// const router = express.Router();

// /**
//  * @swagger
//  * tags:
//  *  name: Employees
//  *  description: API endpoints to manage employees
//  */

// /**
//  * @swagger
//  * /employees:
//  *  get:
//  *      summary: Retrieve a list of employees
//  *      description: Retrieve a list of employees from the database
//  *      tags: [Employees]
//  *      responses:
//  *          '200':
//  *              description: Returns a list of employees
// */
// router.get('/', employeesController.getAllEmployees);

// /**
//  * @swagger
//  * /employees/{empid}:
//  *  get:
//  *      summary: Retrieve an employee by ID
//  *      description: Retrieve an employee from the database by ID
//  *      tags: [Employees]
//  *      parameters:
//  *          - in: path
//  *            name: empid
//  *            required: true
//  *            description: ID of the employee to retrieve
//  *            schema:
//  *              type: integer
//  *      responses:
//  *         '200':
//  *              description: Returns an employee
//  *         '404':
//  *              description: Employee not found
// */
// router.get('/:empid', employeesController.getEmployeeDetails);

// /**
//  * @swagger
//  * /employees:
//  *  post:
//  *      summary: Create a new Employee
//  *      description: Create a new Employee in the database
//  *      tags: [Employees]
//  *      requestBody:
//  *          required: true
//  *          content:
//  *              application/json: 
//  *                  schema:
//  *                      type: object
//  *                      properties:
//  *                          eid:
//  *                              type: integer
//  *                          ename:
//  *                              type: string  
//  *      responses:
//  *          '201':
//  *              description: Employee created successfully
//  *          '500':
//  *              description: Employee creation failed
//  */
// router.post('/', employeesController.createEmployee);

// /**
//  * @swagger
//  * /employees/{empid}:
//  *  put:
//  *      summary: Update an employee by Id
//  *      description: Update an employee in the database by Id
//  *      tags: [Employees]
//  *      parameters: 
//  *          - in: path
//  *            name: empid
//  *            required: true
//  *            description: Id of the employee to edit
//  *            schema:
//  *              type: integer  
//  *      requestBody:
//  *          required: true
//  *          content:
//  *              application/json: 
//  *                  schema:
//  *                      type: object
//  *                      properties:
//  *                          eid:
//  *                              type: integer
//  *                          ename:
//  *                              type: string  
//  *      responses:
//  *          '200':
//  *              description: Employee updated successfully
//  *          '404':
//  *              description: Employee not found
//  */
// router.put('/:empid', employeesController.updateEmployee);

// /**
//  * @swagger
//  * /employees/{empid}:
//  *  delete:
//  *      summary: Delete an employee by Id
//  *      description: Delete an employee in the database by Id
//  *      tags: [Employees]
//  *      parameters: 
//  *          - in: path
//  *            name: empid
//  *            required: true
//  *            description: Id of the employee to edit
//  *            schema:
//  *              type: integer  
//  *      responses:
//  *          '204':
//  *              description: No content
//  *          '404':
//  *              description: Employee not found
//  */
// router.delete('/:empid', employeesController.deleteEmployee);

// export default router;

// ----------------------------------------------------------------------------- Use swagger.json
import express from 'express';
import * as employeeController from '../controllers/employees-controller';

const router = express.Router();

router.get('/', employeeController.getAllEmployees);

router.get('/:empid', employeeController.getEmployeeDetails);

router.post('/', employeeController.createEmployee);

router.put('/:empid', employeeController.updateEmployee);

router.delete('/:empid', employeeController.deleteEmployee);

export default router;