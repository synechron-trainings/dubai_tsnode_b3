import express from 'express';

const router = express.Router();

/**
 * @swagger
 * tags:
 *  name: Users
 *  description: API endpoints to manage users
 */

router.get('/', function (req, res) {});

export default router;