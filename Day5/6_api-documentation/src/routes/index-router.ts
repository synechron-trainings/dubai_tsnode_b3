import express from 'express';
import * as homeController from '../controllers/home-controller';

const router = express.Router();

router.get('/', homeController.renderIndex);

router.get('/about', homeController.renderAbout);

router.get('/contact', homeController.renderContact);

export default router;