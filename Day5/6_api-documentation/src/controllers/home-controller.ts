import { Request, Response } from 'express';

export const renderIndex = (request: Request, response: Response) => {
    response.render('home/index', { pageTitle: 'Home Page' });
}

export const renderAbout = (request: Request, response: Response) => {
    response.render('home/about', { pageTitle: 'About Page' });
}

export const renderContact = (request: Request, response: Response) => {
    response.render('home/contact', { pageTitle: 'Contact Page' });
}
