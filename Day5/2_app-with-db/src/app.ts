import express, { Request, Response } from 'express';
import favicon from 'serve-favicon';
import path from 'path';
import logger from 'morgan';
import 'dotenv/config';
import './config/data-source';

import indexRouter from './routes/index-router';
import employeesRouter from './routes/employees-router';

const app = express();

app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

app.use('/', indexRouter);
app.use('/employees', employeesRouter);

app.use((error: Error, request: Request, response: Response, next: Function) => {
    response.locals.message = error.message;
    response.locals.error = request.app.get('env') === 'development' ? error : {};
    response.status(500);
    response.render('error', { pageTitle: 'Error Page' });
});

export default app;